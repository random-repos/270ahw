#pragma once

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <stdio.h>      // Header file for standard file i/o.
#include <stdlib.h>     // Header file for malloc/free.
#include <cmath>
#include <vector>
#include "VISUconstants.h"
#include "algebra.h"

#define CLAMP(v,l,h)	((v) < (l) ? (l) : (v) > (h) ? (h) : (v))

using namespace std;
using namespace ALGEBRA;

class Vcamera
{
	int mW, mH;
	bool init;
public:
	Vcamera()
	{
		init = false;
	}
	~Vcamera(){}
	void initialize(int aW, int aH)
	{
		//TODO check for stupid values of aW and aH
		init = true;
		mW = aW;
		mH = aH;
	}
	void setCamera()
	{
		if(!init)
			return;
		
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float d = 50;
		float h = mW;
		float w = mH;
		float eyeX 	= w / 2.0;
		float eyeY 	= h / 2.0;
		float fov = 2*atan(eyeY/d);
		float nearDist 	= d / 100.0;
		float farDist 	= d * 100.0;	
		float aspect 			= (float)w/(float)h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov*180/PI, w/h, nearDist, farDist);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(0, 0, d, 0, 0, 0.0, 0.0, 1.0, 0.0);
		
		glScalef(w,h,1);	//flip the y axis and scale sides to 0 to 1
		glTranslatef(-.5,-.5,0);
	}
private:
	
};

class Vdraw
{
    float l,r,u,d;
public:
    Vdraw()
    {
        l = d = 0;
        u = r = 1;
    }
    ~Vdraw()
    {
    }
    void setBoundaries(float al, float ar, float au, float ad)
    {
        l = al;
        r = ar;
        u = au;
        d = ad;
    }

	template<typename T>
    void setBoundariesFromPoints(const T & pts, bool enlarge = false)
    {
        if(pts.size()==0)
        {
            std::cout << "SETTING BOUNDARY FROM EMPTY VECTOR" << std::endl;
            return;
        }
        float maxx, minx, maxy, miny;
        maxx = minx = pts[0].x();
        maxy = miny = pts[0].y();

		if(enlarge)
		{
			maxx = max(maxx,r);
			minx = min(minx,l);
			maxy = max(maxy,u);
			miny = min(miny,d);
		}

        for(int i = 1; i < pts.size(); i++)
        {
            if(pts[i].x() <  minx)
                minx = pts[i].x();
            if(pts[i].x() > maxx)
                maxx = pts[i].x();
            if(pts[i].y() < miny)
                miny = pts[i].y();
            if(pts[i].y() > maxy)
                maxy = pts[i].y();
        }
		l = minx;
		r = maxx;
		u = maxy;
		d = miny;
    }

	template<typename C>
	void setBoundariesFromLines(const C &  pts, const C & vects, bool enlarge = false)
	{
		C r(pts.size());
		for(int i = 0; i < r.size(); i ++)
		{
			r[i] = pts[i] + vects[i];
		}
		setBoundariesFromPoints(r,enlarge);
	}

	template<typename C>
	void drawPoints(const C &  pts)
    {
		glColor3f(1,0,0);
        glBegin(GL_LINE_STRIP);
        for(int i = 0; i < pts.size(); i++)
        {
            glVertex3f((-l + pts[i].x())/(r-l),(-d + pts[i].y())/(u-d),0);
        }
        glEnd();
		glDisable(GL_DEPTH_TEST);
		glBegin(GL_POINTS);
		glColor3f(1,1,1);
        for(int i = 0; i < pts.size(); i++)
        {
            glVertex3f((-l + pts[i].x())/(r-l),(-d + pts[i].y())/(u-d),0);
        }
        glEnd();
		glColor3f(1,0,0);
    }
	template<typename C>
	void drawLines(const C &  pts, const C & vects, int color = 0)
	{
        switch(color)
        {
            case 0:
        		glColor3f(0,1,0);
                break;
            case 1:
                glColor3f(0,0,1);
                break;
            default:
                glColor3f(1,1,1);
                break;
        }
		glBegin(GL_LINES);
        for(int i = 0; i < pts.size(); i++)
        {
			glVertex3f((-l + pts[i].x())/(r-l),(-d + pts[i].y())/(u-d),0);
			glVertex3f((-l + pts[i].x()+vects[i].x())/(r-l),(-d + pts[i].y()+vects[i].y())/(u-d),0);
        }
        glEnd();
	}

};

class Function2D
{
protected:
    float dt, ds;
	int steps;
	float time;
	float S(int index) { return index/(float)(steps-1); }
	float S(float index) { return CLAMP(index,0,steps)/(float)(steps-1); }
	float DS(float leftIndex, float rightIndex) { return S(rightIndex)-S(leftIndex); }
	float IFS(float s) { return (int)(s*(steps-1)); }
public:
	Function2D(float adt, int aSteps):dt(adt),steps(aSteps > 1 ? aSteps : 2),ds(1/(float)(steps-1)),time(0){}
    virtual float tick() { return 0; }
private:
    //set delta t and delta s
    //tick
    //getPointsAtCurrentTime
	
};
class H1PROBLEM2 : private Function2D
{
public:
	std::vector<ALGEBRA::VECTOR_2D<float> > mPts;
	std::vector<ALGEBRA::VECTOR_2D<float> > mDphis;
	std::vector<ALGEBRA::VECTOR_2D<float> > mElastic;
	float k;
	float lKnot;
	H1PROBLEM2(float adt, float ads):Function2D(adt,ads),mPts(steps),mDphis(steps),mElastic(steps)
    {
		k = 1;
		lKnot = 1;
    }
	float psiL(float length)
	{
		return  k * (length-lKnot) * (length-lKnot) / 2 ;
	}
	float phis(float s, float t)
	{
		return s;
	}
	float phit(float s, float t)
	{
		return sin(2*PI*t)*sin(2*PI*s);
	}
	VECTOR_2D<float> phi(float s, float t)
	{
		return ALGEBRA::VECTOR_2D<float>(phis(s,t),phit(s,t));
	}
	float lAtS(float s, float t)
	{
		return dphidss(s,t).Magnitude();
	}
	VECTOR_2D<float> tenAtS(float s, float t)
	{
		return dphidss(s,t) * (k * (lAtS(s,t)-lKnot)/lAtS(s,t));
	}
	VECTOR_2D<float> felasticAtX(float s, float t)
	{
		return (tenAtS(S(IFS(s)+0.5f),t)-tenAtS(S(IFS(s)-0.5f),t)) * (1/DS(IFS(s)-0.5f,IFS(s)+0.5f));
	}
	VECTOR_2D<float> dphidss(float s, float t)
	{
		ALGEBRA::VECTOR_2D<float> v = (phi(S(IFS(s)+0.5f),t) - phi(S(IFS(s)-0.5f),t) )* (1/DS(IFS(s)-0.5f,IFS(s)+0.5f));
		//cout << IFS(s) << endl;
		//cout << (1/DS(IFS(s)-0.5f,IFS(s)+0.5f)) << " - " << S(IFS(s)+0.5f) << " - " << S(IFS(s)-0.5f) << endl;
		return v;
	}
	void evaluate()
	{
		//evaluate at zero
		for(int i = 0; i < steps; i ++)
		{
			mPts[i] = phi(S(i),time);
			mDphis[i] = ALGEBRA::VECTOR_2D<float>(S(i),dphidss(S(i),time).Magnitude());
			mElastic[i] = felasticAtX(S(i),time)*0.1;
		}
	}
    float tick()
    {
		return time += dt;
    }
    virtual std::vector<ALGEBRA::VECTOR_2D<float> > getPoints() 
	{
		evaluate();
		return mPts;
	}
};



class Error
{
};

#define PSHF true
#define MSHF false
class H1PROBLEM3  : private Function2D
{
public:
	VECTOR<VECTOR_2D<float> > mPts;
	VECTOR<VECTOR_2D<float> > mVels;
	VECTOR<VECTOR_2D<float> > mPtsOld;
	VECTOR<VECTOR_2D<float> > mVelsOld;

	VECTOR<VECTOR_2D<float> > mGravityForces;

	VECTOR<VECTOR_2D<float> > mCurrentForces;
    VECTOR<VECTOR_2D<float> > mDampForces;

	VECTOR_2D<float> mG;
	float mKel;
	float mKda;
	float mL0;

	void setInitialConditions()
	{
		for(int i = 0; i < steps; i++)
		{
			mPts[i].x() = S(i);
			mPts[i].y() = 0;
			mVels[i] = VECTOR_2D<float>(0,0);
		}
	}

	H1PROBLEM3(float adt, float ads):Function2D(adt,ads),mPts(steps),mPtsOld(steps),mVels(steps),mVelsOld(steps),mCurrentForces(steps),mDampForces(steps),mGravityForces(steps)
    {
		mG = VECTOR_2D<float>(0,-9.8);
		//mG = VECTOR_2D<float>(0,0);
		for(int i = 0; i < steps; i++)
			mGravityForces(i) = mG;
		mKel = 100;
		mKda = 0.1;
		mL0 = 1;
		setInitialConditions();
    }
	
	float iDS(float index) { return DS(index-0.5f,index+0.5f); }


	//stupid shit
	VECTOR<float> * convertStdVectorVector2dToVector(const VECTOR<VECTOR_2D<float> > v)
	{
		VECTOR<float> * r = new VECTOR<float>(2*v.size());
		for(int i = 0; i < v.size(); i++)
		{
			(*r)(2*i) = v[i].x();
			(*r)(2*i+1) = v[i].y();
		}
		return r;
	} 
	VECTOR<VECTOR_2D<float> > * convertVectorToVectorVector2d(const VECTOR<float> v)
	{
		VECTOR<VECTOR_2D<float> > * r = new VECTOR<VECTOR_2D<float> >(v.Size()/2);
		for(int i = 0; i < v.Size()/2; i++)
		{
			//(*r)(i) = VECTOR_2D<float>(v.getValueAt(2*i),v.getValueAt(2*i+1));
			(*r)(i).x() = v(2*i);
			(*r)(i).y() = v(2*i+1);
		}
		return r;
	}

	//TODO check if ds actually gives you the right number
	//D
	SPARSE_MATRIX<float> * vMassMatrix()
	{
		SPARSE_MATRIX<float> * r = new SPARSE_MATRIX<float>(steps*2,steps*2);

		//NOTE THIS ASSUMES p(s) = 1 AND GIVES TRUE VALUE, WILL NEED TO USE SOME SORT OF DISCRETE INTEGRAL IN THE GENERAL CASE
		for(int i = 1; i < steps-1; i++)
		{
			//we set using these matrices
			/*
			MATRIX_MXN<float> sideMat(2,2);
			sideMat.setScalarMatrix(iDS(i) * 13/6.0f);

			MATRIX_MXN<float> midMat(2,2);
			sideMat.setScalarMatrix(iDS(i) * 2/3.0f);
			
			
			if(i > 1)
				r->SetUsingMatrix(2*i,2*(i-1),sideMat);
			if(i < steps-1)
				r->SetUsingMatrix(2*i, 2*(i+1), sideMat);
			r->SetUsingMatrix(2*i,2*i,midMat);
			
			*/

			//JUST TO BE SAFE we use this for now.
			//TODO use code above
			
			(*r).CreateAndGetAt(2*i,2*i) = iDS(i) * (2/3.0f + 26/6.0f);
			(*r).CreateAndGetAt(2*i+1,2*i+1) = iDS(i) * (2/3.0f + 26/6.0f);

			
		}
		return r;
	}
	SPARSE_MATRIX<float> * vLumpedMassMatrix()
	{
		//NOTE this does not compute the matrix for the endpoints, just sets them to zero
		SPARSE_MATRIX<float> * r = new SPARSE_MATRIX<float>(steps,steps);
		//NOTE THIS ASSUMES p(s) = 1 AND GIVES TRUE VALUE, WILL NEED TO USE SOME SORT OF DISCRETE INTEGRAL IN THE GENERAL CASE
		for(int i = 1; i < steps-1; i++)
		{
			/*
			if(i > 1)
				r.CreateAndGetAt(i,i-1) = ds * 13/6.0f;
			r.CreateAndGetAt(i,i) = ds * 2/3.0f;
			if(i < steps-1)
				r.CreateAndGetAt(i, i+1) = ds * 13/6.0f;
			*/
			//typical case
			//if(i > 1 && i < steps-2)
			if(i > 0 && i < steps-1)
				r->CreateAndGetAt(i,i) = iDS(i) * (2/3.0f + 26/6.0f);
			//endpoint case
			else
				r->CreateAndGetAt(i,i) = iDS(i) * (2/3.0f + 13/6.0f);
		}
		return r;
	}

	//used for undamped not newmarky case
	//nevermind probaly do not want this
	//D
	/*
	VECTOR<float> vMassTranspose()
	{
		SPARSE_MATRIX<float> m = vLumpedMassMatrix();
		VECTOR<float> r(steps);
		for(int i = 0; i < steps; i++)
		{
			//stupid check for endpoints, this is BAD
			if(m(i,i) != 0)
				r(i) = 1/m(i,i);
			else 
				m(i,i) = 0;
		}
		return r;
	}*/

	//ELASTIC FORCES STUFF
	//D
	VECTOR_2D<float> viDerivPos(int index, bool isPlusHalf)
	{
		int i = index;
		if(isPlusHalf)
		{
			return (mPts[i+1] - mPts[i]) * (1/iDS(index+0.5f));
		}
		else
		{
			return (mPts[i] - mPts[i-1]) * (1/iDS(index-0.5f));
		}
	}
	//D
	float viStringLength(int index, bool isPlusHalf)
	{
		return viDerivPos(index,isPlusHalf).Magnitude();
	}
	//D
	VECTOR_2D<float> viTenElastic(int index, bool isPlusHalf)
	{
		float l = viStringLength(index,isPlusHalf);
		return viDerivPos(index,isPlusHalf) * mKel * ((l -mL0)/l);
	}
	//D
	VECTOR<VECTOR_2D<float> > *  vElasticForce()
	{
		VECTOR<VECTOR_2D<float> > * r = new VECTOR<VECTOR_2D<float> >(steps);
		//no forces on end points
		(*r)(0) = (*r)(steps-1) = 0;
		for(int i = 1; i < steps-1; i++)
			(*r)(i) = (viTenElastic(i,PSHF) -  viTenElastic(i,MSHF)) * (1/iDS(i)) ;
		

		for(int i = 0; i < steps; i++)
		{
			mCurrentForces[i] = (*r)(i);
		}
		return r;
	}

	VECTOR<float> *  vfElasticForce()
	{
		VECTOR<VECTOR_2D<float> > * el = vElasticForce();
		VECTOR<float> * r = convertStdVectorVector2dToVector(*el);
		delete el;
		return r;


	}

	//CONJUGATE GRADIENT
	VECTOR<float> conjugateGradient(const VECTOR<float> b, const SPARSE_MATRIX<float> A)
	{
		//x = 0
		VECTOR<float> x(steps*2);

		//r = b - Ax
		VECTOR<float> negax(steps*2);
		A.Multiply(x,negax);
		VECTOR<float> r = negax + b;

		//p = r
		VECTOR<float> p = r;

		//q = Ap
		VECTOR<float> q(steps*2);
		A.Multiply(p,q);

		//gamma = r dot r
		float gam = r.Dot(r);

		//alpha = gamma/(p dot q)
		float pdotq = p.Dot(q);
		assert(pdotq != 0);
		float alp = gam/pdotq;

		int MAX_IT = 10;
		float TOLERANCE = 0.1;
		float err = 9999999;
		for(int i = 0; i < MAX_IT && err > TOLERANCE; i++)
		{
			//x = x+alpha*p
			x += (p*alp);

			//r = r - alpha*q
			r -= (q*alp);
			
			//err = |r|_infinity
			err = r.Max();

			//beta = r dot r / gam
			float bet = r.Dot(r)/gam;

			//gamma = r.Dot(r)
			gam = r.Dot(r);

			//p = beta p + r
			p*= bet;
			p+= r;

			//q = Ap;
			A.Multiply(p,q);

			//alp = gamma/(p dot q)
			alp = gam/p.Dot(q);
		}
		
		return x;
	}



    //DAMPING FORCES
    MATRIX_MXN<float> vDampSubMat(int i, int j)
    {
        MATRIX_MXN<float> r(2,2);
        r(0,0) = (mPts[j].x() - mPts[i].x())*(mPts[j].x() - mPts[i].x());
        r(0,1) = r(1,0) = (mPts[j].x() - mPts[i].x())*(mPts[j].y() - mPts[i].y());
        r(1,1) = (mPts[j].y() - mPts[i].y())*(mPts[j].y() - mPts[i].y());
		float length = (mPts[j] - mPts[i]).Magnitude();
		r *= -mKda/iDS(1)/length/length;
        return r;
    }
	MATRIX_MXN<float> vEmptySubMat()
	{
		MATRIX_MXN<float> r(2,2);
		return r;
	}
	SPARSE_MATRIX<float> * vDampMat()
	{
		SPARSE_MATRIX<float> * r = new SPARSE_MATRIX<float>(2*steps,2*steps);
		for(int i = 0; i < steps; i++)
		{
			
			(*r).SetUsingMatrix(2*i,2*i,
				(i == 0 ? vEmptySubMat() : vDampSubMat(i-1,i))	+=
				(i == steps-1 ? vEmptySubMat() : vDampSubMat(i,i+1))
				);
			if( i > 0)
				(*r).SetUsingMatrix(2*(i-1), 2*i, vDampSubMat(i-1,i)*=(-1));
			if( i < steps-1)
				(*r).SetUsingMatrix(2*(i+1), 2*i, vDampSubMat(i,i+1)*=(-1));
		}
		return r;
	}
	VECTOR<VECTOR_2D<float> > * vDampForces()
	{
		SPARSE_MATRIX<float> * dampMat = vDampMat();
		VECTOR<VECTOR_2D<float> > * r;
		VECTOR<float> * velocities = convertStdVectorVector2dToVector(mVels);
		{
			VECTOR<float> b(steps*2);
			dampMat->Multiply(*velocities,b);
			r = convertVectorToVectorVector2d(b);
		}

		//just for output
        for(int i = 0; i < steps; i++)
            mDampForces[i] = (*r)(i);

		//dampMat->Print();
		delete dampMat;
		delete velocities;
		return r;
	}


	
	void evaluateFE()
	{
		//old stuff
		mPtsOld = mPts;
		mVelsOld = mVels;

		//set endpoints
        
		mPts[0] = VECTOR_2D<float>(sin(PI*time),0);
		mPts[steps-1] = VECTOR_2D<float>(1-sin(PI*time),0);
		mVels[0] = VECTOR_2D<float>(PI*cos(PI*time),0);
		mVels[steps-1] = VECTOR_2D<float>(-PI*cos(PI*time),0);
        

		SPARSE_MATRIX<float> * massMatrix = vLumpedMassMatrix();
		VECTOR<VECTOR_2D<float> > * elasticForces = vElasticForce();
		VECTOR<VECTOR_2D<float> > * dampingForces = vDampForces();;
		//elasticForces->Print();


		//calculate not endpoints
		for(int i = 1; i < steps-1; i ++)
		{	
			mPts[i] = mPtsOld[i] + dt * mVelsOld[i];
			mVels[i] = mVelsOld[i] + dt *  (1/(*massMatrix)(i,i)) *  ( (*elasticForces)(i)  + (*dampingForces)(i) + mG);
		}

		delete dampingForces;
		delete massMatrix;
		delete elasticForces;
	}

	void evaluateNEWMARK()
	{

		//old stuff
		mPtsOld = mPts;
		mVelsOld = mVels;

		//set endpoints
		
		mPts[0] = VECTOR_2D<float>(sin(PI*time),0);
		//mPts[0] = VECTOR_2D<float>(0,sin(PI*time*15)*0.07);
		mPts[steps-1] = VECTOR_2D<float>(1-sin(PI*time),0);
		//techincally you should put this after you calculate the velocities, oh well
		mVels[0] = VECTOR_2D<float>(PI*cos(PI*time),0);
		mVels[steps-1] = VECTOR_2D<float>(-PI*cos(PI*time),0);
		
        
		//compute stuff
		SPARSE_MATRIX<float> * massMatrix = vMassMatrix();
		VECTOR<float > * elasticForces = vfElasticForce();
		VECTOR<float > * gravityForces = convertStdVectorVector2dToVector(mGravityForces);
		VECTOR<float > * velocities = convertStdVectorVector2dToVector(mVelsOld);
		VECTOR<float > massVelocity(steps*2);
		(*massMatrix).Multiply(*velocities,massVelocity);
		SPARSE_MATRIX<float> * dampMat = vDampMat();

		

		//formuate (M-K)vNEW = dt*Fel + M*vOLD + dt*Gravity
		VECTOR<VECTOR_2D<float> > * newVels = convertVectorToVectorVector2d( conjugateGradient((*elasticForces)*dt + massVelocity + (*gravityForces)*dt,(*massMatrix)-(*dampMat)) );
		mVels = (*newVels);
		
		
	
		//calculate not endpoints
		for(int i = 1; i < steps-1; i ++)
			mPts[i] = mPtsOld[i] + dt * mVels[i];




		//no licks
		delete dampMat;
		delete gravityForces;
		delete massMatrix;
		delete elasticForces;
		delete velocities;
		delete newVels;


		
	}

    float tick()
    {
		return time += dt;
    }
};


class P3man
{
	H1PROBLEM3 mProb;
	Vdraw mDraw;
public:
    P3man():mProb(1/10000., 20)
	{
    }
	void printPoints()
	{
		for(int i = 0; i < mProb.mPts.size(); i++)
		{
			cout << mProb.mPts[i].x() << " " << mProb.mPts[i].y() << endl;
		}
	}
    void draw()
    {
		//mProb.evaluateNEWMARK();
		mProb.evaluateFE();
		mDraw.setBoundariesFromPoints<VECTOR<VECTOR_2D<float> > >( mProb.mPts, true);
		mDraw.drawPoints<VECTOR<VECTOR_2D<float> > >(mProb.mPts);
		//mDraw.drawLines(mProb.mPts,mProb.mCurrentForces);
        //mDraw.drawLines(mProb.mPts,mProb.mVels,1);
		//printPoints();
    }
    void tick()
    {
		mProb.tick();
		//cout << "currently at time: " << mProb.tick() << endl;
    }
};

class Pman
{
    H1PROBLEM2 mProb;
	Vdraw mDraw;
public:
    Pman():mProb(1/300., 40)
	{
    }
	void printPoints()
	{
		for(int i = 0; i < mProb.mPts.size(); i++)
		{
			cout << mProb.mPts[i].x() << " " << mProb.mPts[i].y() << endl;
		}
	}
    void draw()
    {
		mProb.evaluate();
		mDraw.setBoundariesFromPoints<vector<VECTOR_2D<float > > >( mProb.mDphis, true);
		mDraw.setBoundariesFromLines(mProb.mPts,mProb.mElastic,true);
		mDraw.drawPoints(mProb.mPts);
		mDraw.drawPoints(mProb.mDphis);
		mDraw.drawLines(mProb.mPts,mProb.mElastic);
		//printPoints();
    }
    void tick()
    {
		cout << "currently at time: " << mProb.tick() << endl;
    }
};

class Vmain
{
    Vcamera mCam;
	P3man mMan;
public:
	Vmain(){}
	~Vmain(){}
	void idleFunc()
	{
        mMan.tick();
        glutPostRedisplay();
	}
    void handleErrors()
    {
        GLenum err = glGetError();
        if(err != GL_NO_ERROR)
        {
            printf("Got glError %s", gluErrorString(err));
        }
    }
    void init(GLsizei width, GLsizei height)
    {
        glDepthFunc(GL_LESS);
        glEnable(GL_DEPTH_TEST);
        glShadeModel(GL_SMOOTH);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        glClearDepth(1.0);
        onResize(width,height);
    }
    void drawScene()
    {
        mCam.setCamera();
		mMan.draw();
        glutSwapBuffers();
    }
    void onResize(GLsizei width, GLsizei height)
    {
        mCam.initialize(width,height);
    }
	void keyPressed(int key, int x, int y)
	{	
	    switch(key)
	    {
		//if you try really hard, the following almost looks like python. Ironically, python does not have a switch statement.
		case ESCAPE:
		    glutDestroyWindow(mainWindowId);
		    break;
		//arrow keys cases
		case UP_ARROW:
		    break;
		case DOWN_ARROW:
		    break;
		case LEFT_ARROW:
		    break;
		case RIGHT_ARROW:
		    break;
		default:
			cout << "tick" << endl;
			mMan.tick();
		    break;
	    }
	    glutPostRedisplay();
	}
private:
};


