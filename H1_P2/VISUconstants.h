#pragma once

//globals
int mainWindowId = 0;

//------------
//input constants
//-----------
#define ESCAPE 27
#define PAGE_UP 73
#define PAGE_DOWN 81
#define UP_ARROW 72
#define DOWN_ARROW 80
#define LEFT_ARROW 75
#define RIGHT_ARROW 77

//-------------
//other constants
//-------------
#define PI 3.1415

//screen constants
GLuint SCREEN_WIDTH = 640;
GLuint SCREEN_HEIGHT = 480;
const GLfloat SCREEN_CLEAR_COLOR[4] = {0.0f,0.0f,0.0f,0.0f};

