#ifndef _algebra_
#define _algebra_

#include <cassert>
#include <iostream>
#include <string>
#include <cstring>
#include "float.h"

namespace ALGEBRA{

template<class T>
class VECTOR_2D{
    T v1,v2;
public:
	VECTOR_2D(const T input):v1(input),v2(input){}
	VECTOR_2D():v1((T)0),v2((T)0){}
	VECTOR_2D(T v1_input,T v2_input):v1(v1_input),v2(v2_input){}
	VECTOR_2D(const VECTOR_2D<T>& v_input):v1(v_input.v1),v2(v_input.v2){}
		
	VECTOR_2D<T>& operator=(const VECTOR_2D<T>& input){v1=input.v1;v2=input.v2;return *this;}

    //PETER DID THIS
	VECTOR_2D<T> operator-(const VECTOR_2D<T>& input) const {
        return VECTOR_2D<T>(v1-input.v1,v2-input.v2);
    }
	VECTOR_2D<T> operator+(const VECTOR_2D<T>& input) const {
        return VECTOR_2D<T>(v1+input.v1, v2+input.v2);
    }
	VECTOR_2D<T> operator*(const T scale) const {
        return VECTOR_2D<T>(v1*scale,v2*scale);
    }
	VECTOR_2D<T> operator-() const {
        return (*this)*(-1);
    }
    //END PETER DID THIS
	
	T operator()(const int component)const {
		assert(component==0 || component==1);
		return component?v2:v1;}
	
	VECTOR_2D<T> Right_Handed_Perp_Vector(){return VECTOR_2D<T>(-v2,v1);}
	
	static T Dot_Product(const VECTOR_2D<T>& u,const VECTOR_2D<T>& v){return u.Dot(v);}
	static T Signed_Triangle_Area(const VECTOR_2D<T>& u,const VECTOR_2D<T>& v){return (T).5*(u.x_copy()*v.y_copy()-v.x_copy()*u.y_copy());}
	static VECTOR_2D<T> ei(const int i){
		assert(i>=0 && i<2);
		if(i==0)
			return VECTOR_2D<T>((T)1,0);
		else
			return VECTOR_2D<T>(0,(T)1);}
	
	T& x() {return v1;} 
	T& y() {return v2;}
	const T& x() const {return v1;} 
	const T& y() const {return v2;}
	
	T x_copy()const{return v1;}
	T y_copy()const{return v2;}
	
	T Dot(const VECTOR_2D<T>& v)const {return v.v1*v1+v.v2*v2;}
	
	T Magnitude(){return sqrt(v1*v1+v2*v2);}
	
	void Normalize(){
		T n=sqrt(v1*v1+v2*v2);
		if(n!=(T)0){
			v1=v1/n;
			v2=v2/n;}}
};


//PETER DID THIS
static VECTOR_2D<double> operator*(const double scale,const VECTOR_2D<double>& input){
    return input*scale;
}
static VECTOR_2D<float> operator*(const float scale,const VECTOR_2D<float>& input){
    return input*scale;
}
//PETER DID THIS

template<class T>
class VECTOR{
    int n;
    T* values;
public:
    VECTOR():n(1) {
        values=new T[n];
		for(int i=0;i<=n-1;i++) values[i]=T();}

    VECTOR(const int n_input):n(n_input) {
        values=new T[n];
		for(int i=0;i<=n-1;i++) values[i]=T();}

    //PETER WROTE THIS
    VECTOR(const VECTOR<T> & v)
    {
        n = v.n;
        values = new T[n];
        for(int i = 0; i < n; i++)
            values[i] = v.values[i];
    }
	//PETER WROTE THESE
	//using these functions is probably rather inefficient
	VECTOR<T> operator*(const float v) const
	{
		VECTOR<T> r(n);
		for(int i = 0; i < n; i++)
			r(i) = (*this)(i)*v;
		return r;
	}
	VECTOR<T> operator+(const VECTOR<T> & v)
	{
		VECTOR<T> r(v.n);
		for(int i = 0; i < n; i++)
			r(i) = (*this)(i) + v(i);
		return r;
	}
	//END PETER WROTE THESE

	//PETER WROTE THESE (efficiently)
	//these are implemented below
	
	VECTOR<T>& operator*=(const float  & v)
	{
		for(int i = 0; i < n; i++)
			(*this)(i) *= v;
		return *this;
	}
	VECTOR<T>& operator+=(const VECTOR<T> & v)
	{
		for(int i = 0; i < n; i++)
			(*this)(i) += v(i);
		return (*this);
	}
	//END PETER WROTE THESE

    ~VECTOR() {delete[] values;}

    bool Resize(const int n_new){
        if (n_new==n) return true;
        assert(n_new>0);
        delete[] values;
        values=new T[n_new];
        n=n_new;
        for(int i=0;i<=n-1;i++) values[i]=T();
        return (values!=NULL);}
	
	VECTOR<T>& operator=(const VECTOR<T>& input){
		assert(input.Size()==this->Size());
		for(int i=0;i<n;i++) values[i]=input.values[i];
		return *this;}
	
	T Dot(VECTOR<T>& x){
		T result=(T)0;
		for(int i=0;i<n;i++) result+=values[i]*x(i);
		return result;
	}
	
	T Min(){
		T min=FLT_MAX;
		for(int i=0;i<n;i++) if(values[i]<min) min=values[i];
		return min;
	}
	
	T Max(){
		T max=-FLT_MAX;
		for(int i=0;i<n;i++) if(values[i]>max) max=values[i];
		return max;
	}
	
    //PETER WROTE THIS
    T& operator()(const int i) {
        return values[i];
    }
	T& operator[](const int i) {
        return values[i];
    }
	T operator()(const int i) const {
        return values[i];
    }
	T operator[](const int i) const {
        return values[i];
    }
	/*
    void operator+=(VECTOR<T>& x) {
        for(int i = 0; i < n; i++)
            values[i] += x(i);
    }*/
	void operator-=(const VECTOR<T>& x) const {
        for(int i = 0; i < n; i++)
            values[i] -= x(i);
    }
    //END PETER WROTE THIS
	
	void Set_To_Zero(){for(int i=0;i<n;i++) values[i]=0;}
	
	T L_inf(){
		T max_norm=(T)0;
		for(int i=0;i<n;i++) if(fabs(values[i])>max_norm) max_norm=fabs(values[i]);
		return max_norm;}
	
	T Sum(){T sum=(T)0;for(int i=0;i<n;i++) sum+=values[i];return sum;}
	
	int Size() const {return n;}
	int size() const {return Size();}
	
	void Enforce_Zero_Sum(){
		T sum=(T)0;
		for(int i=0;i<n;i++) sum+=values[i];
		for(int i=0;i<n;i++) values[i]-=sum/((T)n);}
	
	void Write_DAT_File(std::string file){
		FILE* fpointer;
		fpointer=fopen(file.c_str(),"w");
		for(int i=0;i<n;i++)
			fprintf(fpointer,"%g\n",(double)values[i]);
		fclose(fpointer);}
	
	void Print(){
		std::cout << "Vector =";
		for(int i=0;i<n;i++) std::cout << " " << values[i] << " , ";
		std::cout << "." << std::endl;}
};

template <class T>
std::ostream& operator<<(std::ostream & os, const VECTOR<T>& v){
    os<<"[";for(int i=0;i<v.Size();i++) os<<v(i)<<" ";os<<"]";
    return os;
}
template <class T>
std::ostream& operator<<(std::ostream & os, const VECTOR_2D<T>& v){
    os<<"["<<v.x()<<" "<<v.y()<<"]";
    return os;
}

template<class T>
class SPARSE_ROW{
    const int n;
    int size;
    int* indices;
    T* values;
public:
    SPARSE_ROW(const int n_input):n(n_input),size(0),indices(0),values(0) {}

    //PETER WROTE THIS FUNCTION
    SPARSE_ROW(const SPARSE_ROW<T> & v):n(v.n),size(v.size)
    {
        indices = new int[size];
        memcpy(indices,v.indices,sizeof(int)*size);
        values = new T[size];
        for(int i = 0; i < size; i++)
            values[i] = v.values[i];
    }
	//PETER WROTE THIS FUNCTION
	SPARSE_ROW<T>& operator=(const SPARSE_ROW<T> & v)
	{
		assert(n == v.n);
		delete [] indices;
		delete [] values;

		size = v.size;
		indices = new int[size];
        memcpy(indices,v.indices,sizeof(int)*size);
        values = new T[size];
        for(int i = 0; i < size; i++)
            values[i] = v.values[i];

		return *this;
	}

    ~SPARSE_ROW() {delete[] indices;delete[] values;}
	
	void Zero_Out_Without_Changing_Sparsity(){for(int i=0;i<size;i++) indices[i]=(T)0;}
	
	bool Is_Non_Zero(const int index){for(int i=0;i<size;i++) if(indices[i]==index) return true;return false;}

    T& operator()(const int i){
        assert(0<=i && i<=n-1);
        for(int j=0;j<=size-1;j++) if(indices[j]==i) return values[j];
		assert(false);
		return values[0];}
	
	void Normalize_Row_Sum(){
		T sum=(T)0;
		for(int i=0;i<=size-1;i++) sum+=values[i];
		assert(sum!=0);
		for(int i=0;i<=size-1;i++) values[i]=values[i]/sum;
	}
	
	void Scale(T scale){
		for(int i=0;i<=size-1;i++) values[i]*=scale;}
	
	void Fill_Vector(VECTOR<T>& v){
		assert(v.Size()==n);
		for(int i=0;i<n;i++) v(i)=(T)0;
		for(int i=0;i<size;i++) v(indices[i])=values[i];}
	
	void Print(){
		std::cout << "Sparse Row =";
		for(int i=0;i<n;i++){
			bool found=false;
			for(int j=0;j<=size-1;j++){
				if(indices[j]==i){
					std::cout << " " << values[j] << " , ";
					found=true;}}
			if(!found)
				std::cout << " " << 0 << " , ";}
		std::cout << "." << std::endl;}

	int Number_Nonzero(){return size;}
	
	bool Value_Exists_At_Entry(const int index){
		for(int i=0;i<size;i++) 
			if(indices[i]==index) return true;
		return false;
	}
	
	int Index(const int i_hat){assert(i_hat<size);return indices[i_hat];}
	
	T Value_At_Sparse_Index(const int i_hat){assert(i_hat<size);return values[i_hat];}
	
    T Dot_Product( const VECTOR<T>& v) const {
		assert(v.Size()==n);
        T result=0;for(int i=0;i<=size-1;i++) result+=values[i]*v(indices[i]);
        return result;}

    void Add_Entry(const int index,const T value){
		bool found=false;int entry=0;
		for(int i=0;i<size;i++) if(indices[i]==index){found=true;entry=i;}
		if(found){
			int non_zero_index=indices[entry];
			values[non_zero_index]=value;
			return;}
        size++;int* new_indices=new int[size];T* new_values=new T[size];
        for(int i=0;i<=size-2;i++){
            new_indices[i]=indices[i];new_values[i]=values[i];}
        new_indices[size-1]=index;delete[] indices;indices=new_indices;
        new_values[size-1]=value;delete[] values;values=new_values;}

};


template<class T>
class MATRIX_MXN{
	const int m,n;
	VECTOR<T>** rows;
public:
	MATRIX_MXN(const int m_input,const int n_input):m(m_input),n(n_input){
		rows=new VECTOR<T>*[m];
        for(int i=0;i<=m-1;i++) rows[i]=new VECTOR<T>(n);}

    //PETER WROTE THIS FUNCTION
    MATRIX_MXN(const MATRIX_MXN<T> & v):m(v.m),n(v.n)
    {
		rows=new VECTOR<T>*[m];
		for(int i=0;i<=m-1;i++) rows[i]=new VECTOR<T>(n);
        for(int i = 0; i < m; i++)
            *rows[i] = *v.rows[i];
    }
	
	int M() const {return m;}
	int N() const {return n;}
	
	void Set_To_Zero(){
		for(int i=0;i<m;i++)for(int j=0;j<n;j++) (*this)(i,j)=(T)0;
	}

	//PETER DI THIS ONE
	void setToValue(const T & value)
	{
		for(int i=0;i<m;i++)for(int j=0;j<n;j++) (*this)(i,j)=value;
	}
	//PETER DI THIS ONE
	void setScalarMatrix(const T & value)
	{
		assert(m == n);
		for(int i=0;i<m;i++) (*this)(i,i)=value;
	}
	
	T& operator()(const int i,const int j){
        assert(0<=i && i<=m-1);return (*rows[i])(j);}
    
	const T& operator() (const int i,const int j) const {
        assert(0<=i && i<=m-1);return (*rows[i])(j);}
    

	//PETETR DID THIS
	MATRIX_MXN<T>& operator+=(MATRIX_MXN<T> o)
	{
		assert(m == o.m && n == o.n);
		for(int i = 0; i < m; i++)
			for(int j = 0; j < n; j++)
				(*this)(i,j) += o(i,j);
		return *this;
	}
	MATRIX_MXN<T>& operator*=(T v)
	{
		for(int i = 0; i < m; i++)
			for(int j = 0; j < n; j++)
				(*this)(i,j) *= v;
		return *this;
	}
	//END PETER DID THIS
	
	void Transpose(MATRIX_MXN<T>& result){
		assert(result.M()==n && result.N()==m);
		for(int i=0;i<result.M();i++){
			for(int j=0;j<result.N();j++){
				result(i,j)=(*this)(j,i);}}
	}
	
    void Print(){
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++)
			    std::cout<<(*this)(i,j)<<" ";
			std::cout<<std::endl;}
    }
	//PETER DID THIS
	/*
	SPARSE_MATRIX<T> lump()
	{
		SPARSE_MATRIX<T> r(m,n);
		//for each collumn
		for(int i = 0; i < n; i++)
		{
			r.Row(i).Add_Entry(i,0);
			for( int j = 0; j < m; j++)
			{
				r(i,i) += (*this)(i,j);
			}
		}
		return r;
}*/
    
};
	
static void Multiply(MATRIX_MXN<double>& A, MATRIX_MXN<double>& B, MATRIX_MXN<double>& result){
	assert(A.N()==B.M());
	assert(result.M()==A.M());
	assert(result.N()==B.N());
	for(int i=0;i<result.M();i++){
		for(int j=0;j<result.N();j++){
			result(i,j)=(double)0;
			for(int k=0;k<A.N();k++){
				result(i,j)+=A(i,k)*B(k,j);}}}
}	

static void Multiply(MATRIX_MXN<float>& A, MATRIX_MXN<float>& B, MATRIX_MXN<float>& result){
	assert(A.N()==B.M());
	assert(result.M()==A.M());
	assert(result.N()==B.N());
	for(int i=0;i<result.M();i++){
		for(int j=0;j<result.N();j++){
			result(i,j)=(float)0;
			for(int k=0;k<A.N();k++){
				result(i,j)+=A(i,k)*B(k,j);}}}
}	


template<class T>
class SPARSE_MATRIX{
    const int m,n;
    SPARSE_ROW<T>** rows;
public:
    SPARSE_MATRIX(const int m_input,const int n_input):m(m_input),n(n_input){
        rows=new SPARSE_ROW<T>*[m];
        for(int i=0;i<=m-1;i++) rows[i]=new SPARSE_ROW<T>(n);}

    SPARSE_MATRIX(const SPARSE_MATRIX<T> & v):m(v.m),n(v.n)
    {
		rows=new SPARSE_ROW<T>*[m];
		for(int i=0;i<=m-1;i++) rows[i]=new SPARSE_ROW<T>(n);
        for(int i = 0; i < m; i++)
            *rows[i] = *v.rows[i];
    }

    ~SPARSE_MATRIX() {for(int i=0;i<=m-1;i++) delete rows[i];delete[] rows;}

    void Zero_Out_Without_Changing_Sparsity(){
		for(int i=0;i<m;i++) rows[i]->Zero_Out_Without_Changing_Sparsity();}
	
	T& operator()(const int i,const int j){
        assert(0<=i && i<=m-1);return (*rows[i])(j);}

	T& operator()(const int i,const int j) const{
        assert(0<=i && i<=m-1);return (*rows[i])(j);}

	//PETER WROTE THIS ONE
	bool hasEntry(const int i, const int j) const{
		return (*rows[i]).Value_Exists_At_Entry(j);
	}

	//PETER DID THIS ONE
	T& CreateAndGetAt(const int i,const int j)
	{
        assert(0<=i && i<=m-1);
		if(!rows[i]->Value_Exists_At_Entry(j))
			rows[i]->Add_Entry(j,0);
		return (*rows[i])(j);
	}

	//PETER WROTE THIS ONE
	SPARSE_MATRIX<T> operator-(const SPARSE_MATRIX<T> & mat) const
	{
		assert(mat.N() == n && mat.M() == m);
		//inefficiency cuz it copies this but whatever
		SPARSE_MATRIX<T> r(m,n);
		for(int i = 0; i < m; i++)
			for(int j = 0; j < n; j++)
			{
				T left = hasEntry(i,j) ? (*this)(i,j) : (T)0;
				T right = mat.hasEntry(i,j) ? mat(i,j) : (T)0;
				r.CreateAndGetAt(i,j) = left-right;
			}
		return r;
	}

	

	
	void Column(const int j,VECTOR<T>& c){
		assert(j>=0 && j<n && c.Size()==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ai=Row(i);
			if(Ai.Is_Non_Zero(j)) c(i)=Ai(j);
			else c(i)=(T)0;}}
	
	void Normalize_Row_Sums(){
		for(int i=0;i<m;i++) Row(i).Normalize_Row_Sum();
	}
	
	void Right_Multiply(SPARSE_MATRIX<T>& B,SPARSE_MATRIX<T>&AB){
		assert(n==B.M());
		VECTOR<T> column(B.M());
		for(int i=0;i<AB.M();i++){
			for(int j=0;j<B.N();j++){
				B.Column(j,column);
				SPARSE_ROW<T>& Ai=Row(i);
				SPARSE_ROW<T>& ABi=AB.Row(i);
				ABi(j)=Ai.Dot_Product(column);}}} 
	
	void Scale_Rows(T scale){
		for(int i=0;i<=m-1;i++) rows[i]->Scale(scale);}
	
	void Print(){
		std::cout << "Sparse Matrix = " << std::endl;
		for(int i=0;i<m;i++)
			Row(i).Print();}

    SPARSE_ROW<T>& Row(const int i) const {
        assert(0<=i && i<=m-1);return *rows[i];}
	
	void Residual(VECTOR<T>& rhs,VECTOR<T>& x,VECTOR<T>& r)
	{for(int i=0;i<m;i++){
		SPARSE_ROW<T>& Ai=Row(i);
		r(i)=rhs(i)-Ai.Dot_Product(x);}}
	
	void Multiply(const VECTOR<T>& x,VECTOR<T>& b) const {//as in b=Ax
		assert(b.Size()==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ai=Row(i);
			b(i)=Ai.Dot_Product(x);}}
	
	T A_Norm_Squared(const VECTOR<T>& x){
		T result;
		assert(x.Size()==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ai=Row(i);
			result+=Ai.Dot_Product(x)*x(i);}
		return result;
	}
	
	int M() const {return m;}
	
	int N() const {return n;}
	
	/*
	void Multiply(const VECTOR<T>& x,VECTOR<T>& b)
	{for(int i=0;i<m;i++){
		SPARSE_ROW<T>& ri=Row(i);
		b(i)=ri.Dot_Product(x);}}*/
	
	void Transpose(SPARSE_MATRIX<T>& transpose)
	{
		assert(transpose.m==n && transpose.n==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ri=Row(i);
			for(int j_hat=0;j_hat<Ri.Number_Nonzero();j_hat++){
				int j=Ri.Index(j_hat);
				SPARSE_ROW<T>& Pj=transpose.Row(j);
				Pj.Add_Entry(i,Ri.Value_At_Sparse_Index(j_hat));}}
	}
	
	void Write_DAT_File(std::string file){
		FILE* fpointer;
		fpointer=fopen(file.c_str(),"w");
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& row=Row(i);
			for(int j=0;j<n;j++){
				bool found=false;
				if(row.Value_Exists_At_Entry(j)){
						fprintf(fpointer,"%g ",(*this)(i,j));
					found=true;}
				if(!found) fprintf(fpointer,"%g ",(T)0);}
			fprintf(fpointer,"\n");}
		fclose(fpointer);
	}

   //PETERDID THIS
    //sets submatrix starting at (inclusive) row i, col j, 
    void SetUsingMatrix(int ai, int aj, const MATRIX_MXN<T> & mat)
    {
        for(int i = 0; i <  mat.M(); i++)
            for(int j = 0; j <  mat.N(); j++) 
                (*this).CreateAndGetAt(i+ai, j+aj) = mat(i,j);

    }
};
	
}
#endif
