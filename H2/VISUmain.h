#pragma once

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <stdio.h>      // Header file for standard file i/o.
#include <stdlib.h>     // Header file for malloc/free.
#include <cstdlib>
#include <cmath>
#include <vector>
#include "VISUconstants.h"
#include "algebra.h"
#include "utilities.h"
#include "VISUh1.h"
#include "VISUh2.h"
#include "grid.h"
#include "multigrid.h"
#define CLAMP(v,l,h)	((v) < (l) ? (l) : (v) > (h) ? (h) : (v))

using namespace std;
using namespace ALGEBRA;


class Vmain
{
    Vcamera mCam;
	VISUrotozoom mRot;
	P2_33man mMan;
    bool mIsOn;
public:
	Vmain(){}
	~Vmain(){}
	void idleFunc()
	{
        mRot.update();
        if(mIsOn)
            glutPostRedisplay();
	}
    void handleErrors()
    {
        GLenum err = glGetError();
        if(err != GL_NO_ERROR)
        {
            printf("Got glError %s", gluErrorString(err));
        }
    }
    void init(GLsizei width, GLsizei height)
    {
        glDepthFunc(GL_LESS);
        glEnable(GL_DEPTH_TEST);
        glShadeModel(GL_SMOOTH);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        glClearDepth(1.0);
        onResize(width,height);
    }
    void drawScene()
    {
		mCam.setRotation(mRot.getX(),mRot.getY(),mRot.getZ());
        mCam.setCamera();
        mMan.tick();
		mMan.draw();
        glutSwapBuffers();
    }
    void onResize(GLsizei width, GLsizei height)
    {
        mCam.initialize(width,height);
    }
	void keyReleased(int key, int x, int y)
	{
		mRot.keyReleased(key);
	}
	void keyPressed(int key, int x, int y)
	{	
		mRot.keyPressed(key);
	    switch(key)
	    {
		//if you try really hard, the following almost looks like python. Ironically, python does not have a switch statement.
		case ESCAPE:
		    glutDestroyWindow(mainWindowId);
			std::exit(0);
		    break;
		//arrow keys cases
		case UP_ARROW:
		    break;
		case DOWN_ARROW:
		    break;
		case LEFT_ARROW:
		    break;
		case RIGHT_ARROW:
		    break;
        case RETURN:
            mIsOn = !mIsOn;
		case SPACE:
			mMan.tick();
		default:
            glutPostRedisplay();
		    break;
	    }
	}
private:
};


