#pragma once

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <stdio.h>      // Header file for standard file i/o.
#include <stdlib.h>     // Header file for malloc/free.
#include <cstdlib>
#include <cmath>
#include <vector>
#include "VISUconstants.h"
#include "algebra.h"
#include "utilities.h"
#include "multigrid.h"
#include "grid.h"

#define CLAMP(v,l,h)	((v) < (l) ? (l) : (v) > (h) ? (h) : (v))

using namespace std;
using namespace ALGEBRA;


template <typename T>
class MULTIGRID_SOLVER
{
    int depth;
    int N;  //NOTE this should be of the form M*2^k some m some k
    int * nH;
    T dxFine; //deltax at fine level

    VECTOR<T>** vH;
	VECTOR<T>** tH;
	VECTOR<T>** rH;
    SPARSE_MATRIX<T>** aM;
	SPARSE_MATRIX<T>** pM;
	SPARSE_MATRIX<T>** rM;

	VECTOR<T> * mSol;
	
	int getMaxPossibleDepth()
	{
		int s = N;
		int counter = 0;
		while(s%2 == 0 && s > 0)
		{
			s/=2;
			counter++;
		}
		return counter;
	}
    int getSize(int d = 0)
    {
        int s = N;
        for(int i = 0; i < d; i++)
        {
            assert(N%2 == 0);
            s /= 2;
        }
        return s;
    }
public:
    ~MULTIGRID_SOLVER()
    {
        delete [] nH;
        //TODO prevent leaks here
    }
	void setRhs(VECTOR<T> & aRhs)
	{
		for(int i = 0; i < depth; i++)
        {
			if(i == 0)
				(*rH[i]) = aRhs;
			else
				(*rM[i-1]).Multiply(*rH[i-1],*rH[i]);
		}
	}
	void setSolution(VECTOR<T> & aSol)
	{
		mSol = &aSol;
	}
	MULTIGRID_SOLVER(int aN, SPARSE_MATRIX<T> & aA, int maxDepth=99)
		:N(aN)
	{
		depth = min(maxDepth,getMaxPossibleDepth());
        vH = new VECTOR<T>*[depth];	//Au = b the u part
        tH = new VECTOR<T>*[depth]; //residuals b-Au
        rH = new VECTOR<T>*[depth];	//Au = b <- rhs part
        aM = new SPARSE_MATRIX<T>*[depth];
        pM = new SPARSE_MATRIX<T>*[depth-1];
        rM = new SPARSE_MATRIX<T>*[depth-1];
        nH = new int[depth];

        for(int i = 0; i < depth; i++)
        {
            nH[i] = getSize(i);
            int s = nH[i];	
            vH[i] = new VECTOR<T>(s);
            tH[i] = new VECTOR<T>(s);
            rH[i] = new VECTOR<T>(s);
            aM[i] = new SPARSE_MATRIX<T>(s,s);
            if(i > 0)
            {
                rM[i-1] = new SPARSE_MATRIX<T>(s,nH[i-1]);
                pM[i-1] = new SPARSE_MATRIX<T>(nH[i-1],s);
                //now actually construct them
                for(int r = 0; r < s; r++)
				{
					if(r == 0) (*rM[i-1]).CreateAndGetAt(r, 2*s-1) = 0.25;
					else (*rM[i-1]).CreateAndGetAt(r, 2*r-1) = 0.25;
					(*rM[i-1]).CreateAndGetAt(r, 2*r) = 0.5;
					(*rM[i-1]).CreateAndGetAt(r, (2*r+1)%nH[i-1]) = 0.25;
				}
				for(int r = 0; r < s; r++)
				{
					if(r == 0) (*pM[i-1]).CreateAndGetAt(2*s-1,r) = .5;
					else (*pM[i-1]).CreateAndGetAt(2*r-1,r) = .5;
					(*pM[i-1]).CreateAndGetAt(2*r,r) =  1;
					(*pM[i-1]).CreateAndGetAt((2*r+1)%nH[i-1],r) = .5;
				}
				//(*rM[i-1]).Print();
				//(*pM[i-1]).Print();
            }
			//create our first aM
            if(i == 0)
                (*aM[0]) = aA;
            else
            {
                //want R^iA
				SPARSE_MATRIX<T> tempMatrix(nH[i],nH[i-1]);
                (*rM[i-1]).Right_Multiply(*aM[i-1],tempMatrix);
				tempMatrix.Right_Multiply(*pM[i-1],*aM[i]);
            }
			cout << "setup depth: " << i << endl;
        }
	}
	MULTIGRID_SOLVER(int aN, VECTOR<T> & aRhs, VECTOR<T> & aSol, SPARSE_MATRIX<T> & aA, int maxDepth=99)
		:N(aN)
    {
		setSolution(aSol);
		depth = min(maxDepth,getMaxPossibleDepth());
        vH = new VECTOR<T>*[depth];	//Au = b the u part
        tH = new VECTOR<T>*[depth]; //residuals b-Au
        rH = new VECTOR<T>*[depth];	//Au = b <- rhs part
        aM = new SPARSE_MATRIX<T>*[depth];
        pM = new SPARSE_MATRIX<T>*[depth-1];
        rM = new SPARSE_MATRIX<T>*[depth-1];
        nH = new int[depth];

        for(int i = 0; i < depth; i++)
        {
            nH[i] = getSize(i);
            int s = nH[i];	
            vH[i] = new VECTOR<T>(s);
            tH[i] = new VECTOR<T>(s);
            rH[i] = new VECTOR<T>(s);
            aM[i] = new SPARSE_MATRIX<T>(s,s);
            if(i > 0)
            {
                rM[i-1] = new SPARSE_MATRIX<T>(s,nH[i-1]);
                pM[i-1] = new SPARSE_MATRIX<T>(nH[i-1],s);
                //now actually construct them
                for(int r = 0; r < s; r++)
				{
					if(r == 0) (*rM[i-1]).CreateAndGetAt(r, 2*s-1) = 0.25;
					else (*rM[i-1]).CreateAndGetAt(r, 2*r-1) = 0.25;
					(*rM[i-1]).CreateAndGetAt(r, 2*r) = 0.5;
					(*rM[i-1]).CreateAndGetAt(r, (2*r+1)%nH[i-1]) = 0.25;
				}
				for(int r = 0; r < s; r++)
				{
					if(r == 0) (*pM[i-1]).CreateAndGetAt(2*s-1,r) = .5;
					else (*pM[i-1]).CreateAndGetAt(2*r-1,r) = .5;
					(*pM[i-1]).CreateAndGetAt(2*r,r) =  1;
					(*pM[i-1]).CreateAndGetAt((2*r+1)%nH[i-1],r) = .5;
				}
				//(*rM[i-1]).Print();
				//(*pM[i-1]).Print();
            }
			//create our first aM
            if(i == 0)
                (*aM[0]) = aA;
            else
            {
                //want R^iA
				SPARSE_MATRIX<T> tempMatrix(nH[i],nH[i-1]);
                (*rM[i-1]).Right_Multiply(*aM[i-1],tempMatrix);
				tempMatrix.Right_Multiply(*pM[i-1],*aM[i]);
            }
			cout << "setup depth: " << i << endl;
        }
		setRhs(aRhs);
    }
	
	//jacobi
	void smoothJ(SPARSE_MATRIX<T> * A, VECTOR<T> * rhs, VECTOR<T> * v, VECTOR<T> * temp)
	{
		//temp = r, v = u, rhs = b
		A->Residual(*rhs,*v,*temp);
		for(int i = 0; i < rhs->size(); i++)
        {
			if(A->hasEntry(i,i)){
				//in the zero case, there is nothing we can do to fix this equation
				if(fabs((*A)(i,i)) != 0)
				{
					float sig = (*temp)(i)/(*A)(i,i);
					(*v)(i) += sig*2/3;
				}
			}
        }
		//recalculate residuals
		A->Residual(*rhs,*v,*temp);
	}
	//GS
    void smoothGS(SPARSE_MATRIX<T> * A, VECTOR<T> * rhs, VECTOR<T> * v, VECTOR<T> * temp)
    {
		//temp = r, v = u, rhs = b
		A->Residual(*rhs,*v,*temp);
		for(int i = 0; i < rhs->size(); i++)
        {
			if(A->hasEntry(i,i)){
				//in the zero case, there is nothing we can do to fix this equation
				if((*A)(i,i) != 0 )
				{
					float sig = (*temp)(i)/(*A)(i,i);
					(*v)(i) += sig;
					//recalculate residuals
					A->Residual(*rhs,*v,*temp);
				}
			}
        }
		
    }
	void smooth(SPARSE_MATRIX<T> * A, VECTOR<T> * rhs, VECTOR<T> * v, VECTOR<T> * temp)
	{
		for(int i = 0; i < 1; i++)
			smoothJ(A,rhs,v,temp);
	}
	//note, these two fnuctions are probably incorrect
	void restrict(VECTOR<T> * arg, int deep, VECTOR<T> * result)
	{
		for(int i = 0; i < nH[deep+1]; i++)
		{
			int levels = 2*i;
			(*result)(i) = (*arg)(INDEX(i,nH[deep+1]).Index_Periodic())*0.25;
			(*result)(i) += (*arg)(INDEX(i-1,nH[deep+1]).Index_Periodic())*0.25;
			(*result)(i) += (*arg)(INDEX(i+1,nH[deep+1]).Index_Periodic())*0.5;
		}
	}
	void prolongate(VECTOR<T> * arg, int deep, VECTOR<T> * result)
	{
		for(int i = 0; i < nH[deep-1]; i++)
		{
			if(i%2)
			{
				(*result)(i) = (*arg)(INDEX(i/2,nH[deep-1]).Index_Periodic())*0.25;
				(*result)(i) += (*arg)(INDEX(i/2+1,nH[deep-1]).Index_Periodic())*0.25;
			}
			else
				(*result)(i) = (*arg)(INDEX(i/2,nH[deep-1]).Index_Periodic())*0.5;
		}
	}
	
    void vCycle()
    {
		for(int i = 0; i < depth; i++)
		{
			vH[i]->Set_To_Zero();
			tH[i]->Set_To_Zero();
		}
        for(int i = 0; i < depth-1; i++)
        {
            //smooth
            smooth(aM[i],rH[i],vH[i],tH[i]);
			//cout << "residual at " << i << "th down step is: " << tH[i]->L_inf() << endl;
            //restrict and store results
			//Rr = RA(u0 - u)
			rM[i]->Multiply(*tH[i],*rH[i+1]);
			//restrict(tH[i],i,rH[i+1]);
			//initial guess needs to be zero
			vH[i+1]->Set_To_Zero();
        }
		for(int i = depth-1; i > 0; i--)
        {
			//smooth the bottom
			smooth(aM[i],rH[i],vH[i],tH[i]);
            //prolongate your results
			//prolongate(vH[i],i,tH[i-1]);
			pM[i-1]->Multiply(*vH[i],*tH[i-1]);
			//add correction
			*vH[i-1] += *tH[i-1];
			//smooth
            
        }
		//smooth one more time
		for(int i = 0; i < 1; i++)
		{
			smooth(aM[0],rH[0],vH[0],tH[0]);
			//cout << "residual at last step is: " << tH[0]->L_inf() << endl;
			//*vH[0] += *tH[0];
		}

		//set the solution
		*mSol = *vH[0];
    }

};



class H2PROBLEM3 : private Function2D
{
public:
	//u_t + ac*u_x = 0
	//has solution
	//u(x) = g(x + at);
	VECTOR<VECTOR_2D<float> > mPts;		//graphs (x,u(x))
	VECTOR<VECTOR_2D<float> > mErrorBase;
	VECTOR<VECTOR_2D<float> > mError;
	VECTOR<VECTOR_2D<float> > mSupposeToBe;
	float ac;
	H2PROBLEM3(float ads, float _a = 0.1):Function2D(3*_a/(ads-1),ads),mPts(ads),mError(ads),mErrorBase(ads),mSupposeToBe(ads)	//TODO may need to pass in ads+1 so ds gets computed right, or maybe you just don't care
    {
		ac = _a;
		//initial conditions
		for(int i = 0; i < steps; i++)
		{
			mErrorBase[i] = VECTOR_2D<float>(S(i),0);
			mPts[i] = mSupposeToBe[i] = VECTOR_2D<float>(S(i), sin(2*PI*S(i)));
		}
    }

	int getIndex(int ind)
	{
		while(ind < 0)
			ind += steps;
		return ind % steps;
	}
	void evaluateNONSEMILAGRANGIAN()
	{
		if(time > 5) return;
		VECTOR<VECTOR_2D<float> > mPtsNew(steps);
		int check = ac > 0 ? -1 : 1;
		for(int i = 0; i < steps; i++)
		{
			//mPtsNew[i] = mPts[i] - ac * (dt / DS()) * ( check * mPts[getIndex(i+ check)] - check * mPts[i]);
			mPtsNew[i] = VECTOR_2D<float>(S(i) , mPts[i].y() - ac * (dt / DS()) * ( check * mPts[getIndex(i+ check)].y() - check * mPts[i].y()));
		}
		for(int i = 0; i < steps; i++)
		{
			mPts[i] = mPtsNew[i];
			mSupposeToBe[i] = VECTOR_2D<float>(S(i),sin(2*PI*(S(i) - ac * time)));
			mError[i] = mPts[i] - mSupposeToBe[i];
		}

	}	

    float tick()
    {
		return time += dt;
    }
	/*
    virtual std::vector<ALGEBRA::VECTOR_2D<float> > getPoints() 
	{
	}*/
};

class P2_3man
{
	H2PROBLEM3 mProb;
	Vdraw mDraw;
public:
    P2_3man():mProb(160)
	{
    }
    void draw()
    {
		mProb.evaluateNONSEMILAGRANGIAN();
		mDraw.setBoundariesFromPoints<VECTOR<VECTOR_2D<float> > >( mProb.mPts, false);
		mDraw.setBoundariesFromPoints<VECTOR<VECTOR_2D<float> > >( mProb.mSupposeToBe, true);
		mDraw.drawPoints<VECTOR<VECTOR_2D<float> > >(mProb.mPts);
		mDraw.drawLines<VECTOR<VECTOR_2D<float> > >(mProb.mErrorBase,mProb.mError,1);
		mDraw.drawPoints<VECTOR<VECTOR_2D<float> > >(mProb.mSupposeToBe,1);
    }
    void tick()
    {
		mProb.tick();
    }
};

class Function25Dper
{
protected:
	int NS;
	int N;
	float time,dt;
	INDEX_2D ind;
public:
	void setIndex(int i, int j)	{ind = INDEX_2D(i,j,N,N);}
	int getIndex() { return ind.Index_Periodic(); }
	INDEX_2D getIndex2d() { return ind; }
	int getIndex(int i, int j){
		INDEX_2D tind = INDEX_2D(i,j,N,N);
		return tind.Index_Periodic();
	}
	INDEX_2D getIndex2d( VECTOR_2D<float> pos)
	{
		while(pos.y() < 0) pos.y() += 1;
		while(pos.x() < 0) pos.x() += 1;
		//we just assume the floor as the closest point, technically we should round this appropriately
		int ihat = ((int)floor(pos.x()/DS()))%N;
		int jhat = ((int)floor(pos.y()/DS()))%N;
		return INDEX_2D(ihat,jhat,N,N);
	}
	VECTOR_2D<float> S() { return VECTOR_2D<float>(ind.i_Periodic()/(float)N, ind.j_Periodic()/(float)N); }
	VECTOR_2D<float> S(int i, int j){
		INDEX_2D tind = INDEX_2D(i,j,N,N);
		return VECTOR_2D<float>(tind.i_Periodic()/(float)N, tind.j_Periodic()/(float)N);
	}
	float DS() { return 1/float(N); }
	
	
	Function25Dper(float adt, float ads):dt(adt),time(0),NS(ads*ads),N(ads)
	{
	}
};


class H2PROBLEM33 : private Function25Dper
{
public:
	int getN() { return N; }
	VECTOR<float> mPts;		//graphs (x,u(x))
	VECTOR<VECTOR_2D<float> > mGrid;
	VECTOR<float> mSupposeToBe;
	VECTOR<float> mError;
    SPARSE_MATRIX<float> mStar;
	int mCounter;
	void initialize()
	{
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				//mPts(getIndex()) = 0.3*rand()/RAND_MAX;
                //mPts(getIndex()) = 0;
				if(i == 79)
					mPts(getIndex()) = -1;
				if(i == 81)
					mPts(getIndex()) = 1;
				mSupposeToBe(getIndex()) = trueP(i,j);
				mGrid(getIndex()) = S();
			}	
		}
	}
	H2PROBLEM33(float adt,float ads):
        Function25Dper(adt,ads),
        mPts(ads*ads),mError(ads*ads),
        mSupposeToBe(ads*ads),
        mGrid(ads*ads),
        mStar(ads*ads,ads*ads)
    {
		initialize();
		mCounter = 0;
        //construct the star matrix thingy
        float hs = DS()*DS();
        for(int i = 0; i < N; i++)
        {
             for(int j = 0; j < N; j++)
            {
				INDEX_2D ij(i,j,N,N);
				INDEX_2D ijp1(i,j+1,N,N);
				INDEX_2D ijm1(i,j-1,N,N);
				INDEX_2D ip1j(i+1,j,N,N);
				INDEX_2D im1j(i-1,j,N,N);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ij.Index_Periodic(),4/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ijp1.Index_Periodic(),-1/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ijm1.Index_Periodic(),-1/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ip1j.Index_Periodic(),-1/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(im1j.Index_Periodic(),-1/hs);
            }
        }
    }
	float f(int i, int j)
	{
		return 8*PI*sin(2*PI*S(i,j).x())*cos(2*PI*S(i,j).y());
	}
    float trueP(int i, int j)
    {
		return (1/PI)*sin(2*PI*S(i,j).x())*cos(2*PI*S(i,j).y());
    }
	void evaluate()
	{
		VECTOR< float > rhs(N*N);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
                //true value
                mSupposeToBe(getIndex()) = trueP(i,j);
				//Ax=b, construct the b part
                rhs(getIndex()) = f(i,j);
			}
		}
		initialize();
		mCounter++;
        //now solve for x (using conjugate gradient Cfor now)
        CONJUGATE_GRADIENT<float> cg(mStar,mPts,rhs,mCounter);
        cg.Solve(true);

		/*MULTIGRID_SOLVER<float> mg(NS,rhs,mPts,mStar);
		mg.vCycle();*/

		/*
		MULTIGRID_POISSON<float> mg(5,N,2,2);
		mg.Fine_Level_RHS() = rhs;
		mg.Fine_Level_V() = mPts;
		mg.Solve();
		mPts = mg.Fine_Level_V();*/
		
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
                //true value
                mError(getIndex()) = abs(mSupposeToBe(getIndex()) - mPts(getIndex()));
			}
		}
	}	

    float tick()
    {
		return time += dt;
    }
	/*
    virtual std::vector<ALGEBRA::VECTOR_2D<float> > getPoints() 
	{
	}*/
};


class P2_33man
{
	H2PROBLEM33 mProb;
	V3Draw mDraw;
public:
    P2_33man():mProb(1/300.,160)
	{
		mProb.evaluate();
    }
	void draw()
    {
		mDraw.drawGrid<VECTOR<VECTOR_2D<float> >,VECTOR<float> >(mProb.mGrid,mProb.mSupposeToBe,mProb.getN(),mProb.getN());
		mDraw.drawGrid<VECTOR<VECTOR_2D<float> >,VECTOR<float> >(mProb.mGrid,mProb.mPts,mProb.getN(),mProb.getN(),1);
		//mDraw.drawGrid<VECTOR<VECTOR_2D<float> >,VECTOR<float> >(mProb.mGrid,mProb.mError,mProb.getN(),mProb.getN(),2);
    }
    void tick()
    {
		mProb.evaluate();
		mProb.tick();
    }
};

class H2PROBLEM4 : private Function25Dper
{ 
public:
    int getN() { return N; }
	VECTOR_2D<float> a;
	VECTOR< float > mVals;	//actual wvalue of function 
	VECTOR<VECTOR_2D<float> > mGrid;	//input arghumen
	H2PROBLEM4(float ads, VECTOR_2D<float> _a) : 
		a(_a),
		Function25Dper(3*_a.Magnitude()/float(ads-1),ads),
		mVals(ads*ads),
		mGrid(ads*ads)
    {
		//initial conditions
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				if(BOX<float>(0.25,0.75,0.75,0.25).intersects( S() ))
				{
					mVals(getIndex()) = 1;
				}
				else mVals(getIndex()) = 0;
			
				mGrid(getIndex()) = S();
			}	
		}
    }

	void evaluate()
	{
		VECTOR< float > mValsNew(N*N);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				//compute point for approximation
				VECTOR_2D<float> poshat = mGrid(getIndex()) - a*dt;
				//compute interpolation points
				int ihat = floor(poshat.x()/DS());
				int jhat = floor(poshat.y()/DS());
				//determine interpolation vars
				float xint = (poshat.x() - ihat*DS())/DS();
				float yint = (poshat.y() - jhat*DS())/DS();
				//bilinear interpolate
				mValsNew(getIndex()) = 
					(1-xint)*(1-yint)*mVals(getIndex(ihat,jhat))
					+ xint*(1-yint)*mVals(getIndex(ihat+1,jhat))
					+ (1-xint)*yint*mVals(getIndex(ihat,jhat+1))
					+ yint*xint*mVals(getIndex(ihat+1,jhat+1));
			}
		}
		mVals = mValsNew;
	}	

    float tick()
    {
		return time += dt;
    }
};

class P2_4man
{
	H2PROBLEM4 mProb;
	V3Draw mDraw;
public:
    P2_4man():mProb(160,VECTOR_2D<float>(0.25,0.1))
	{
    }
    void draw()
    {
		mProb.evaluate();
        mDraw.drawGrid<VECTOR<VECTOR_2D<float> >,VECTOR<float> >(mProb.mGrid,mProb.mVals,mProb.getN(),mProb.getN());  
    }
    void tick()
    {
		mProb.tick();
    }
};


class H2PROBLEM5 : private Function25Dper
{
	float phi(const VECTOR_2D<float> & x)
	{
		if((x-VECTOR_2D<float>(0.25,0.25)).Magnitude() - 0.25 < 0)
		{
			return 256*pow((x-VECTOR_2D<float>(0.25,0.25)).Magnitude()-0.25,4);
		}
		else if((x-VECTOR_2D<float>(0.75,0.75)).Magnitude() - 0.1 < 0)
		{
			return 10000*pow((x-VECTOR_2D<float>(0.75,0.75)).Magnitude()-0.1,4);
		}
		else
			return 0;
	}
	VECTOR_2D<float> g(VECTOR_2D<float> pos)
	{
		//return VECTOR_2D<float>(0.5-rand()/(float)RAND_MAX,0.5-rand()/(float)RAND_MAX);
		float val = 0.0001;
		VECTOR_2D<float> psy (val/2.,0);
		VECTOR_2D<float> psx (0,val/2.);
		//zero on boundaries usually	
		//if(pos.x() < val || pos.x() > 1-val || pos.y() < val || pos.y() > 1-val)
		//	return VECTOR_2D<float>(0,0);
		return VECTOR_2D<float>( (phi(pos+psy)-phi(pos-psy)), -(phi(pos+psx)-phi(pos-psx)) )*(1/val);	
	}
	VECTOR_2D<float> g(int i, int j)
	{
		return g(S(i,j));
	}
	void setMultigrid()
	{
		//construct the star matrix thingy
        float hs = DS()*DS();
        for(int i = 0; i < N; i++)
        {
             for(int j = 0; j < N; j++)
            {
				INDEX_2D ij(i,j,N,N);
				INDEX_2D ijp1(i,j+1,N,N);
				INDEX_2D ijm1(i,j-1,N,N);
				INDEX_2D ip1j(i+1,j,N,N);
				INDEX_2D im1j(i-1,j,N,N);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ij.Index_Periodic(),4/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ijp1.Index_Periodic(),-1/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ijm1.Index_Periodic(),-1/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(ip1j.Index_Periodic(),-1/hs);
				mStar.Row(ij.Index_Periodic()).Add_Entry(im1j.Index_Periodic(),-1/hs);
            }
        }
		mMg = NULL;
		//mMg = new MULTIGRID_SOLVER<float>(NS,mStar);
	}
public:
	int getN() { return N; }

	int Ns; //discretizations in our solid

	GRIDS::GRID_2D_MAC_X<float> yMac;
	GRIDS::GRID_2D_MAC_X<float> xMac;

	VECTOR<VECTOR_2D<float> > mPts;		//graphs (x,u(x))
	VECTOR<VECTOR_2D<float> > mGrid, mParticles;

	VECTOR<VECTOR_2D<float> > mUs; //uStar for advection step
	VECTOR<float> mUn; //x velocities
	VECTOR<float> mVn; //y velocities

	SPARSE_MATRIX<float> mAx, mAy, mAtx, mAty; //our two interpolation matrices and their transposes

	MULTIGRID_SOLVER<float> * mMg;
	SPARSE_MATRIX<float> mStar; 
	MULTIGRID_POISSON<float> mMgT;

	~H2PROBLEM5(){ if(mMg != NULL) delete mMg;}
	
	float phi4(float x)
	{
		float mag = abs(x);
		if(mag > 2)
			return 0;
		else if ( mag > 1)
			return (1/8.) * (5-2*mag - sqrt(-7 + 12*mag - 4 * mag * mag));
		else
			return (1/8.) * (3-2*mag + sqrt(1 + 4*mag - 4 * mag * mag));
	}
	float delta4(VECTOR_2D<float> pos)
	{

		float x = pos.x()/DS();
		float y = pos.y()/DS();
		return (1/DS()/DS())*phi4(x)*phi4(y);
	}

	float interpolateVn(VECTOR_2D<float> poshat)
	{
		poshat.x() -= 1/(float)N/(float)2;
		if(poshat.x() < 0) poshat.x() += 1;
		//compute interpolation points
		int ihat = floor(poshat.x()/DS());
		int jhat = floor(poshat.y()/DS());
		//cout << ihat << " " << jhat << " " << getIndex(ihat,jhat) << endl;
		//determine interpolation vars
		float xint = (poshat.x() - ihat*DS())/DS();
		float yint = (poshat.y() - jhat*DS())/DS();
		//bilinear interpolate
		return 
			(1-xint)*(1-yint)*mVn(getIndex(ihat,jhat))
			+ xint*(1-yint)*mVn(getIndex(ihat+1,jhat))
			+ (1-xint)*yint*mVn(getIndex(ihat,jhat+1))
			+ yint*xint*mVn(getIndex(ihat+1,jhat+1));
	}
	float interpolateUn(VECTOR_2D<float> poshat)
	{
		poshat.y() -= 1/(float)N/(float)2;
		if(poshat.y() < 0) poshat.y() += 1;
		//compute interpolation points
		int ihat = floor(poshat.x()/DS());
		int jhat = floor(poshat.y()/DS());
		//cout << ihat << " " << jhat << " " << getIndex(ihat,jhat) << endl;
		//determine interpolation vars
		float xint = (poshat.x() - ihat*DS())/DS();
		float yint = (poshat.y() - jhat*DS())/DS();
		//bilinear interpolate
		return 
			(1-xint)*(1-yint)*mUn(getIndex(ihat,jhat))
			+ xint*(1-yint)*mUn(getIndex(ihat+1,jhat))
			+ (1-xint)*yint*mUn(getIndex(ihat,jhat+1))
			+ yint*xint*mUn(getIndex(ihat+1,jhat+1));
	}
	H2PROBLEM5(float adt,float ads, int d = 1/*number of discretizatios*/)
		:Function25Dper(adt,ads),
		mPts(ads*ads),mGrid(ads*ads),mParticles(ads*ads),
		mUn(ads*ads),mVn(ads*ads),mUs(ads*ads),
		xMac(ads, 1/ads, 0, 1/ads/2.),yMac(ads,1/ads, 1/ads/2., 0),
		mStar(N*N,N*N),
		Ns(d),mAx(d,N*N), mAy(d,N*N), mAtx(N*N,d), mAty(N*N,d)
		,mMgT(1,N,2,1)
    {
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				mPts(getIndex()) = g(i,j);
				mGrid(getIndex()) = mParticles(getIndex()) = S();
				//mUn(getIndex()) = g(xMac.Index(getIndex2d())).x();
				//mVn(getIndex()) = g(yMac.Index(getIndex2d())).y();
				
			}	
		}
		setMultigrid();
	}
	void advection_update_u_star(VECTOR<float> & newUn)
	{
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				//compute velocity at current xMac grid position
				VECTOR_2D<float> adjustedPosition = S(i,j);
				adjustedPosition.y() += 1/(float)N/(float)2;
				VECTOR_2D<float> vel(mUn(getIndex()),interpolateVn(adjustedPosition));
				newUn(getIndex()) = interpolateUn((adjustedPosition - vel*dt));
			}	  
		}
	}
	void advection_update_v_star(VECTOR<float> & newVn)
	{
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				//compute velocity at current xMac grid position
				VECTOR_2D<float> adjustedPosition = S(i,j);
				adjustedPosition.x() += 1/(float)N/(float)2;
				VECTOR_2D<float> vel(mVn(getIndex()),interpolateUn(adjustedPosition));
				newVn(getIndex()) = interpolateVn((adjustedPosition - vel*dt));
			}	  
		}
	}
	void compute_divergence_and_solve(VECTOR<float> & starUn,VECTOR<float> & starVn)
	{
		//laplace p_np1 = div u* / dt;
		//compute div u*
		//create RHS
		VECTOR<float> rhs(NS);
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				rhs(getIndex()) = 
					(starUn(getIndex(i+1,j)) - starUn(getIndex(i,j)) +
					starVn(getIndex(i,j+1)) - starVn(getIndex(i,j)))/dt/DS();
			}}
		VECTOR<float> answer(NS);
		//solve using CG for now

		
		/*CONJUGATE_GRADIENT<float> cg(mStar,rhs,answer,1000);
		cg.Solve(true);
		cout << endl;*/
		
		
		mMgT.Fine_Level_RHS() = rhs;
		mMgT.Fine_Level_V();
		mMgT.Solve();
		answer = mMgT.Fine_Level_V();

		//or using my awesome multigrid class
		//mMg->setRhs(rhs);
		//mMg->setSolution(answer);
		//mMg->vCycle();

		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				mUn(getIndex()) = starUn(getIndex()) + dt*( answer(getIndex(i,j)) - answer(getIndex(i-1,j)) )/DS();
				mVn(getIndex()) = starVn(getIndex()) + dt*( answer(getIndex(i,j)) - answer(getIndex(i,j-1)) )/DS();
			}
		}
	}
	void periodicPosition(VECTOR_2D<float> & pos)
	{
			while(pos.x() < 0)
				pos.x() += 1;
			while(pos.y() < 0)
				pos.y() += 1;
			while(pos.x() > 1)
				pos.x() -= 1;
			while(pos.y() > 1)
				pos.y() -= 1;
	}
	void evaluateSolid(VECTOR<VECTOR_2D<float> > & position, VECTOR<VECTOR_2D<float> > & sol, VECTOR<float> & xF, VECTOR<float> & yF, float stepSize)
	{
		mAx = SPARSE_MATRIX<float>(Ns,NS);
		mAy = SPARSE_MATRIX<float>(Ns,NS);
		mAtx = SPARSE_MATRIX<float>(NS,Ns);
		mAty = SPARSE_MATRIX<float>(NS,Ns);
		//compute our matrices
		VECTOR_2D<float> offset(0,0);
		for(int s = 0; s < Ns; s++)
		{
			//get the position of the particle
			VECTOR_2D<float> intPos = position(s) + offset;
			periodicPosition(intPos);
			INDEX_2D cent = getIndex2d(intPos);
			for(int i = cent.i_Periodic() -2; i <= cent.i_Periodic() + 2; i++)
			{
				for(int j = cent.j_Periodic() -2; j <= cent.j_Periodic()+ 2; j++)
				{
					//mAx.CreateAndGetAt(s,getIndex(i,j)) = mUn(getIndex(i,j)) * delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(xMac.X_Min(),xMac.X_Max()) - intPos) * DS();
					//mAy.CreateAndGetAt(s,getIndex(i,j)) = mVn(getIndex(i,j)) * delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(yMac.Y_Min(),yMac.Y_Max()) - intPos) * DS();

					//without using the matrices for now
					float ydelta = delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(yMac.X_Min(),yMac.Y_Min()) - intPos);
					float xdelta = delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(xMac.X_Min(),xMac.Y_Min()) - intPos);
					mAtx.CreateAndGetAt(getIndex(i,j),s) = mAx.CreateAndGetAt(s,getIndex(i,j)) = xdelta;
					mAty.CreateAndGetAt(getIndex(i,j),s) = mAy.CreateAndGetAt(s,getIndex(i,j)) = ydelta;
				}
			}
		}
		//advect
		VECTOR<float> starUn(NS);
		VECTOR<float> starVn(NS);
		advection_update_u_star(starUn);
		advection_update_v_star(starVn);
	
		//forward euler step using new forces
		VECTOR<float> xFmac(NS);
		VECTOR<float> yFmac(NS);
		mAtx.Multiply(xF,xFmac);
		mAty.Multiply(yF,yFmac);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				starUn(getIndex()) += dt*stepSize*(1/DS()/DS())*xFmac(getIndex());
				starVn(getIndex()) += dt*stepSize*(1/DS()/DS())*yFmac(getIndex());
			}
		}

		//solve for pressure
		compute_divergence_and_solve(starUn,starVn);

		
		//upload solution
		VECTOR<float> xFs(Ns);
		VECTOR<float> yFs(Ns);
		mAx.Multiply(mUn,xFs);
		mAy.Multiply(mVn,yFs);
		for(int i = 0; i < Ns; i++)
			sol(i) = DS()*DS()*VECTOR_2D<float>(xFs(i),yFs(i));

		
		//set marker particles
		for(int s = 0; s < NS; s++)
		{
			//get the position of the particle
			VECTOR_2D<float> intPos = mParticles(s);
			INDEX_2D cent = getIndex2d(intPos);
			for(int i = cent.i_Periodic() -2; i <= cent.i_Periodic() + 2; i++)
			{
				for(int j = cent.j_Periodic() -2; j <= cent.j_Periodic()+ 2; j++)
				{
					float ydelta = DS()*DS()*delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(yMac.X_Min(),yMac.Y_Min()) - intPos);
					float xdelta = DS()*DS()*delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(xMac.X_Min(),xMac.Y_Min()) - intPos);
					mParticles(s) += VECTOR_2D<float>( dt * mUn(getIndex(i,j)) * ydelta,
						dt * mVn(getIndex(i,j)) * xdelta);
				}
			}
		}
	}
	void evaluate()
    {
        //advection step using mac grid
		VECTOR<float> starUn(NS);
		VECTOR<float> starVn(NS);
		advection_update_u_star(starUn);
		advection_update_v_star(starVn);
		compute_divergence_and_solve(starUn,starVn);


		//TODO our lagrangian data, need to get this somehow
		//construct Ax and Ay interpolation matrices using delta function

		//this is really stupid, you should just reallocate the matrices if your going to do this
		//mAx.Zero_Out_Without_Changing_Sparsity();
		//mAy.Zero_Out_Without_Changing_Sparsity();
		
		//mAx = SPARSE_MATRIX<float>(NS,NS);
		//mAy = SPARSE_MATRIX<float>(NS,NS);

		
		for(int s = 0; s < NS; s++)
		{
			//get the position of the particle
			VECTOR_2D<float> intPos = mParticles(s);
			INDEX_2D cent = getIndex2d(intPos);
			for(int i = cent.i_Periodic() -2; i <= cent.i_Periodic() + 2; i++)
			//for(int i = 0; i < N; i++)
			{
				for(int j = cent.j_Periodic() -2; j <= cent.j_Periodic()+ 2; j++)
				//for(int j = 0; j < N; j++)
				{
					//mAx.CreateAndGetAt(s,getIndex(i,j)) = mUn(getIndex(i,j)) * delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(xMac.X_Min(),xMac.X_Max()) - intPos) * DS();
					//mAy.CreateAndGetAt(s,getIndex(i,j)) = mVn(getIndex(i,j)) * delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(yMac.Y_Min(),yMac.Y_Max()) - intPos) * DS();

					//without using the matrices for now
					float ydelta = DS()*DS()*delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(yMac.X_Min(),yMac.Y_Min()) - intPos);
					float xdelta = DS()*DS()*delta4(mGrid(getIndex(i,j)) + VECTOR_2D<float>(xMac.X_Min(),xMac.Y_Min()) - intPos);
					mParticles(s) += VECTOR_2D<float>( dt * mUn(getIndex(i,j)) * ydelta,
						dt * mVn(getIndex(i,j)) * xdelta);
				}
			}
		}

		//finally bilinear interpolate the points back in
		//check
		/*
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				setIndex(i,j);
				//mPts(getIndex()) = VECTOR_2D<float>(xMac.Interpolate(mUn,S().x(),S().y()),yMac.Interpolate(mVn,S().x(),S().y()));
				mPts(getIndex()) = VECTOR_2D<float>(interpolateUn(S()),interpolateVn(S()));
				//mPts(getIndex()) = VECTOR_2D<float>(mUn(getIndex()),mVn(getIndex()));

				VECTOR_2D<float> pt = mParticles(getIndex());
				mParticles(getIndex()) = VECTOR_2D<float>(pt.x() + dt*interpolateUn(pt),pt.y() + dt*interpolateVn(pt));
			}
		}*/
	}	
    float tick(){return time += dt;}
};

class P2_5man
{
	H2PROBLEM5 mProb;
	V3Draw mDraw;
public:
    P2_5man():mProb(1/90.,32,32*32)
	{
    }
	void draw()
    {
		mProb.evaluate();
		//mDraw.drawLines<VECTOR<VECTOR_2D<float> > >(mProb.mGrid,mProb.mPts);
		mDraw.drawPoints<VECTOR<VECTOR_2D<float> > >(mProb.mParticles);
    }
    void tick()
    {
		mProb.tick();
    }
};