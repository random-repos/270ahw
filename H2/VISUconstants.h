#pragma once

//globals
int mainWindowId = 0;

//------------
//input constants
//-----------
#define SPACE 32
#define RETURN 13
#define ESCAPE 27
#define PAGE_UP 104
#define PAGE_DOWN 105
#define UP_ARROW 101
#define DOWN_ARROW 103
#define LEFT_ARROW 100
#define RIGHT_ARROW 102

//-------------
//other constants
//-------------
#define PI 3.1415

//screen constants
GLuint SCREEN_WIDTH = 640;
GLuint SCREEN_HEIGHT = 480;
const GLfloat SCREEN_CLEAR_COLOR[4] = {0.0f,0.0f,0.0f,0.0f};

