//---------
//system library includes
//---------

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <stdio.h>      // Header file for standard file i/o.
#include <stdlib.h>     // Header file for malloc/free.
#include <cmath>
#include "VISUconstants.h"
#include "VISUmain.h"

Vmain man;
GLvoid keyPressed(unsigned char key, int x, int y)
{
	man.keyPressed(key, x, y);
}
GLvoid keyReleased(unsigned char key, int x, int y)
{
	man.keyReleased(key,x,y);
}
GLvoid specialKeyPressed(int key, int x, int y)
{
	man.keyPressed(key, x, y);
}
GLvoid specialKeyReleased(int key, int x, int y)
{
	man.keyReleased(key, x, y);
}
GLvoid drawScene()
{
    man.drawScene();
}
GLvoid onResize(GLsizei width, GLsizei height)
{
    man.onResize(width,height);
}

GLvoid idleFunc()
{
	man.idleFunc();
}


GLvoid initGl(GLsizei width, GLsizei height)
{
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    glClearDepth(1.0);
    onResize(width,height);
}

int main(int argc, char **argv) 
{  
	std::cout << "initialize glut" << std::endl;
    glutInit(&argc, argv);  
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);  
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);  
    glutInitWindowPosition(0, 0);  
    mainWindowId = glutCreateWindow("270A");

    glutKeyboardFunc(&keyPressed);
	glutKeyboardUpFunc(&keyReleased);
	glutSpecialFunc(&specialKeyPressed);
	glutSpecialUpFunc(&specialKeyReleased);
    glutReshapeFunc(&onResize);
    glutDisplayFunc(&drawScene);  
    glutIdleFunc(&idleFunc);
    
    man.init(SCREEN_WIDTH,SCREEN_HEIGHT);

    glutMainLoop();
    return 1;
}
