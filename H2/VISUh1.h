#pragma once

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>      // Header File For The OpenGL32 Library
#include <GL/glu.h>     // Header File For The GLu32 Library
#include <stdio.h>      // Header file for standard file i/o.
#include <stdlib.h>     // Header file for malloc/free.
#include <cstdlib>
#include <cmath>
#include <vector>
#include "VISUconstants.h"
#include "algebra.h"
#include "utilities.h"
#include "VISUh2.h"

#define CLAMP(v,l,h)	((v) < (l) ? (l) : (v) > (h) ? (h) : (v))

using namespace std;
using namespace ALGEBRA;

#define PSHF true
#define MSHF false
class H1PROBLEM3  : private Function2D
{
public:
	VECTOR<VECTOR_2D<float> > mPts;
	VECTOR<VECTOR_2D<float> > mVels;
	VECTOR<VECTOR_2D<float> > mPtsOld;
	VECTOR<VECTOR_2D<float> > mVelsOld;

	VECTOR<VECTOR_2D<float> > mGravityForces;

	VECTOR<VECTOR_2D<float> > mCurrentForces;
    VECTOR<VECTOR_2D<float> > mDampForces;

	VECTOR_2D<float> mG;
	float mKel;
	float mKda;
	float mL0;

	H2PROBLEM5 fluidSolver;

	void setInitialConditions()
	{
		for(int i = 0; i < steps; i++)
		{
			mPts[i].x() = S(i)/4.+0.25;
			mPts[i].y() = 0.8 - S(i)/4.;
			mVels[i] = VECTOR_2D<float>(0,0);
		}
	}

	H1PROBLEM3(float adt, float ads):Function2D(adt,ads),mPts(steps),mPtsOld(steps),mVels(steps),mVelsOld(steps),mCurrentForces(steps),mDampForces(steps),mGravityForces(steps),
		fluidSolver(adt,ads*2,ads)
    {
		//mG = VECTOR_2D<float>(0,-9.8);
		mG = VECTOR_2D<float>(0,0);
		for(int i = 0; i < steps; i++)
			mGravityForces(i) = mG;
		mKel = 100;
		mKda = 0.1;
		mL0 = 0.9;
		setInitialConditions();
    }
	
	float iDS(float index) { return DS(index-0.5f,index+0.5f); }


	//stupid shit
	VECTOR<float> * convertStdVectorVector2dToVector(const VECTOR<VECTOR_2D<float> > v)
	{
		VECTOR<float> * r = new VECTOR<float>(2*v.size());
		for(int i = 0; i < v.size(); i++)
		{
			(*r)(2*i) = v[i].x();
			(*r)(2*i+1) = v[i].y();
		}
		return r;
	} 
	VECTOR<VECTOR_2D<float> > * convertVectorToVectorVector2d(const VECTOR<float> v)
	{
		VECTOR<VECTOR_2D<float> > * r = new VECTOR<VECTOR_2D<float> >(v.Size()/2);
		for(int i = 0; i < v.Size()/2; i++)
		{
			//(*r)(i) = VECTOR_2D<float>(v.getValueAt(2*i),v.getValueAt(2*i+1));
			(*r)(i).x() = v(2*i);
			(*r)(i).y() = v(2*i+1);
		}
		return r;
	}

	//TODO check if ds actually gives you the right number
	//D
	SPARSE_MATRIX<float> * vMassMatrix()
	{
		SPARSE_MATRIX<float> * r = new SPARSE_MATRIX<float>(steps*2,steps*2);

		//NOTE THIS ASSUMES p(s) = 1 AND GIVES TRUE VALUE, WILL NEED TO USE SOME SORT OF DISCRETE INTEGRAL IN THE GENERAL CASE
		for(int i = 0; i < steps; i++)
		{
			(*r).CreateAndGetAt(2*i,2*i) = iDS(i) * (2/3.0f + 26/6.0f);
			(*r).CreateAndGetAt(2*i+1,2*i+1) = iDS(i) * (2/3.0f + 26/6.0f);
		}
		return r;
	}
	SPARSE_MATRIX<float> * vLumpedMassMatrix()
	{
		//NOTE this does not compute the matrix for the endpoints, just sets them to zero
		SPARSE_MATRIX<float> * r = new SPARSE_MATRIX<float>(steps,steps);
		//NOTE THIS ASSUMES p(s) = 1 AND GIVES TRUE VALUE, WILL NEED TO USE SOME SORT OF DISCRETE INTEGRAL IN THE GENERAL CASE
		for(int i = 0; i < steps; i++)
		{
			if(i > 0 && i < steps-1)
				r->CreateAndGetAt(i,i) = iDS(i) * (2/3.0f + 26/6.0f);
			else
				r->CreateAndGetAt(i,i) = iDS(i) * (2/3.0f + 13/6.0f);
		}
		return r;
	}
	//ELASTIC FORCES STUFF
	//D
	VECTOR_2D<float> viDerivPos(int index, bool isPlusHalf)
	{
		int i = index;
		if(isPlusHalf)
		{
			return (mPts[i+1] - mPts[i]) * (1/iDS(index+0.5f));
		}
		else
		{
			return (mPts[i] - mPts[i-1]) * (1/iDS(index-0.5f));
		}
	}
	//D
	float viStringLength(int index, bool isPlusHalf)
	{
		return viDerivPos(index,isPlusHalf).Magnitude();
	}
	//D
	VECTOR_2D<float> viTenElastic(int index, bool isPlusHalf)
	{
        //this is broken !
        if(index == 0 && !isPlusHalf)
            return VECTOR_2D<float>();
        if(index == steps-1 && isPlusHalf)
            return VECTOR_2D<float>();

		float l = viStringLength(index,isPlusHalf);
		return viDerivPos(index,isPlusHalf) * mKel * ((l -mL0)/l);
	}
	//D
	VECTOR<VECTOR_2D<float> > *  vElasticForce()
	{
		VECTOR<VECTOR_2D<float> > * r = new VECTOR<VECTOR_2D<float> >(steps);
		//no forces on end points
		(*r)(0)  = 0;
		(*r)(steps-1) = 0;
		for(int i = 1; i < steps-1; i++)
        {
			
            int k = i;
			/*
            if(i == 0)
                k = 1;
            else if ( i == steps-1)
                k = steps-2;*/
			

			(*r)(i) = (viTenElastic(k,PSHF) -  viTenElastic(k,MSHF)) * (1/iDS(k)) ;
        }
		

		for(int i = 0; i < steps; i++)
		{
			mCurrentForces[i] = (*r)(i);
		}
		return r;
	}

	VECTOR<float> *  vfElasticForce()
	{
		VECTOR<VECTOR_2D<float> > * el = vElasticForce();
		VECTOR<float> * r = convertStdVectorVector2dToVector(*el);
		delete el;
		return r;


	}

	//CONJUGATE GRADIENT
	VECTOR<float> conjugateGradient(const VECTOR<float> b, const SPARSE_MATRIX<float> A)
	{
		//x = 0
		VECTOR<float> x(steps*2);

		//r = b - Ax
		VECTOR<float> negax(steps*2);
		A.Multiply(x,negax);
		VECTOR<float> r = negax + b;

		//p = r
		VECTOR<float> p = r;

		//q = Ap
		VECTOR<float> q(steps*2);
		A.Multiply(p,q);

		//gamma = r dot r
		float gam = r.Dot(r);

		//alpha = gamma/(p dot q)
		float pdotq = p.Dot(q);
		assert(pdotq != 0);
		float alp = gam/pdotq;

		int MAX_IT = 20;
		float TOLERANCE = 0.01;
		float err = 9999999;
		for(int i = 0; i < MAX_IT && err > TOLERANCE; i++)
		{
			//x = x+alpha*p
			x += (p*alp);

			//r = r - alpha*q
			r -= (q*alp);
			
			//err = |r|_infinity
			err = r.Max();

			//beta = r dot r / gam
			float bet = r.Dot(r)/gam;

			//gamma = r.Dot(r)
			gam = r.Dot(r);

			//p = beta p + r
			p*= bet;
			p+= r;

			//q = Ap;
			A.Multiply(p,q);

			//alp = gamma/(p dot q)
			alp = gam/p.Dot(q);
		}
		
		return x;
	}



    //DAMPING FORCES
    MATRIX_MXN<float> vDampSubMat(int i, int j)
    {
        MATRIX_MXN<float> r(2,2);
        r(0,0) = (mPts[j].x() - mPts[i].x())*(mPts[j].x() - mPts[i].x());
        r(0,1) = r(1,0) = (mPts[j].x() - mPts[i].x())*(mPts[j].y() - mPts[i].y());
        r(1,1) = (mPts[j].y() - mPts[i].y())*(mPts[j].y() - mPts[i].y());
		float length = (mPts[j] - mPts[i]).Magnitude();
		r *= -mKda/iDS(1)/length/length;
        return r;
    }
	MATRIX_MXN<float> vEmptySubMat()
	{
		MATRIX_MXN<float> r(2,2);
		return r;
	}
	SPARSE_MATRIX<float> * vDampMat()
	{
		SPARSE_MATRIX<float> * r = new SPARSE_MATRIX<float>(2*steps,2*steps);
		for(int i = 0; i < steps; i++)
		{
			
			(*r).SetUsingMatrix(2*i,2*i,
				(i == 0 ? vEmptySubMat() : vDampSubMat(i-1,i))	+=
				(i == steps-1 ? vEmptySubMat() : vDampSubMat(i,i+1))
				);
			if( i > 0)
				(*r).SetUsingMatrix(2*(i-1), 2*i, vDampSubMat(i-1,i)*=(-1));
			if( i < steps-1)
				(*r).SetUsingMatrix(2*(i+1), 2*i, vDampSubMat(i,i+1)*=(-1));
		}
		return r;
	}
	VECTOR<VECTOR_2D<float> > * vDampForces()
	{
		SPARSE_MATRIX<float> * dampMat = vDampMat();
		VECTOR<VECTOR_2D<float> > * r;
		VECTOR<float> * velocities = convertStdVectorVector2dToVector(mVels);
		{
			VECTOR<float> b(steps*2);
			dampMat->Multiply(*velocities,b);
			r = convertVectorToVectorVector2d(b);
		}

		//just for output
        for(int i = 0; i < steps; i++)
            mDampForces[i] = (*r)(i);

		//dampMat->Print();
		delete dampMat;
		delete velocities;
		return r;
	}

	VECTOR_2D<float> closestPointOnSegment(int pt, int seg)
	{
		//PAGE 9 of notes
		VECTOR_2D<float> An = (mPts[seg+1] - mPts[seg]);
		An.Normalize();
		VECTOR_2D<float> Bn = mPts[pt] - mPts[seg];
		VECTOR_2D<float> p = An.Dot(Bn)*An;
		return mPts[seg] + p;
	}

	float computeLambdaOfPointAndSegment(int pt, int seg)
	{
		//PAGE 9 of notes
		VECTOR_2D<float> An = (mPts[seg+1] - mPts[seg]);
		VECTOR_2D<float> Bn = mPts[pt] - mPts[seg];
		float lambda;
		float proj = An.Dot(Bn)/(An.Magnitude()*An.Magnitude());
		if(proj < 0)
			lambda = 0;
		else if(proj > 1)
			lambda = 1;
		else lambda = proj;
		return lambda;
	}
	VECTOR_2D<float> closestPointOnSegmentClamped(int pt, int seg)
	{
		float lambda = computeLambdaOfPointAndSegment(pt,seg);
		return (1-lambda) * mPts[seg] + lambda * mPts[seg+1];
	}

	float distanceFromSegment(int pt, int seg)
	{
		return (closestPointOnSegment(pt,seg)-mPts[pt]).Magnitude();
	}

    //checks if there is collision between pt and seg where seg is the line segment seg to seg+1
    bool isCollisionCROSS(int pt, int seg)
    {
        //TODO
		//stupid not using area of triangle method
		//before time step of one
		VECTOR_2D<float> An = mPts[seg+1] - mPts[seg];
		VECTOR_2D<float> Bn = mPts[pt] - mPts[seg];
		VECTOR_2D<float> Anp = mPts[seg+1] + dt*mVels[seg+1] - (mPts[seg] + dt * mVels[seg]);
		VECTOR_2D<float> Bnp = mPts[pt] + dt*mVels[pt] - (mPts[seg] + dt * mVels[seg]);
		
		float c1 = crossProduct(An,Bn);
		float c2 = crossProduct(Anp,Bnp);
		float DISTANCE_THRESHOLD = 0.1;
		if(distanceFromSegment(pt,seg) < DISTANCE_THRESHOLD)
		{
			if( (c1 < 0 && c2 > 0) || c1 > 0 && c2 < 0)
				return true;
		}

		//after time step of one
        return false;
    }

	bool checkCollisionInTime(int pt, int seg, float time)
	{
		float EPSILON2 = 0.00001;	
		if(time >= 0 && time <= dt)
		{
			VECTOR_2D<float> nx0 = mPts[seg] + time * mVels[seg];
			VECTOR_2D<float> nx1 = mPts[seg+1] + time * mVels[seg+1];
			VECTOR_2D<float> nx2 = mPts[pt] + time * mVels[pt];
			VECTOR_2D<float> An = nx1-nx0;
			VECTOR_2D<float> Bn = nx2-nx0;
			float lambda;
			float proj = An.Dot(Bn)/(An.Magnitude()*An.Magnitude());
			if(proj < 0)
				lambda = 0;
			else if(proj > 1)
				lambda = 1;
			else lambda = proj;
			VECTOR_2D<float> closestPoint = (1-lambda) * nx0 + lambda * nx1;
			if( (nx2-closestPoint).Magnitude() < EPSILON2)
			{
				//cout << (nx2-closestPoint).Magnitude() << endl;
				return true;
			}
		}
		return false;
	}

	bool isCollision(int pt, int seg)
	{
		float EPSILON1 = 0;	//b squared minus 4 ac = 0
		float EPSILON2 = 0;	//constants = to 0
		
		VECTOR_2D<float> x0 = mPts[seg];
		VECTOR_2D<float> x1 = mPts[seg+1];
		VECTOR_2D<float> x2 = mPts[pt];
		VECTOR_2D<float> v0 = mVels[seg];
		VECTOR_2D<float> v1 = mVels[seg+1];
		VECTOR_2D<float> v2 = mVels[pt];
		//want area as at^2 + bt + c
		//A_x dot B_y - A_y dot B_x
		//A_x dot B_y
		float axdotby_a = (v1.x()-v0.x())*(v2.y()-v0.y());
		float axdotby_b = (x1.x()-x0.x())*(v2.y()-v0.y()) + (x2.y()-x0.y())*(v1.x()-v0.x());
		float axdotby_c = (x2.y()-x0.y())*(x1.x()-x0.x());
		//A_y dot B_x
		float aydotbx_a = (v1.y()-v0.y())*(v2.x()-v0.x());
		float aydotbx_b = (x1.y()-x0.y())*(v2.x()-v0.x()) + (x2.x()-x0.x())*(v1.y()-v0.y());
		float aydotbx_c = (x2.x()-x0.x())*(x1.y()-x0.y());

		float ra = axdotby_a - aydotbx_a;
		float rb = axdotby_b - aydotbx_b;
		float rc = axdotby_c - aydotbx_c;
		//(-b +- sqrt(b^2-4ac))/2a
		float bsm4ac = rb*rb-4*ra*rc;
		if (abs(bsm4ac) < EPSILON1)
			bsm4ac = 0;
		if (bsm4ac < 0) 
			return false;
		float sqrtbsm4ac = sqrt(bsm4ac); 
		//linear case
		if(abs(ra) <= EPSILON2)
		{
			//cout << ra << "*t^2 + " << rb << "*t + " << rc << endl;
			if(abs(rb) <= EPSILON2)
				return false;
			float r = -rc/rb;
			if(checkCollisionInTime(pt,seg,r))
			{
				return true;
			}
			return false;
		}

		float r1 = (-rb + sqrtbsm4ac)/((float)2*ra);
		float r2 = (-rb - sqrtbsm4ac)/((float)2*ra);
		if(checkCollisionInTime(pt,seg,r2))
		{
			//cout << pt << " " << seg << endl;
			return true;
		}
		if(checkCollisionInTime(pt,seg,r1))
		{
			//cout << pt << " " << seg << endl;
			return true;
		}
		return false;
	}

	void addRepulsion(int pt, int seg)
	{
		float lambda = computeLambdaOfPointAndSegment(pt,seg);
		VECTOR_2D<float> vhat = (1-lambda)*mVels[seg] + lambda * mVels[seg+1];
		VECTOR_2D<float> n = mPts[pt] - closestPointOnSegment(pt,seg);
		n.Normalize();
		VECTOR_2D<float> relVel = (mVels[pt] - vhat).Dot(n) * n;
		VECTOR_2D<float> I = -relVel*(1/(1 + lambda * lambda + (1-lambda)*(1-lambda)));
		mVels[pt] += I;
		mVels[seg] -= (1-lambda)*I;
		mVels[seg+1] -= (lambda)*I;
	}

    void computeCollisionsSlow()
    {
        int maxIter = 10;
        for(int ct = 0; ct < maxIter; ct++)
        {
			//for each point
            for(int i = 1; i < steps; i++)
			{
				//for each segment
				for(int j = 0; j < steps-1; j++)
				{ 
					if(i != j && i != j+1)
						if(isCollision(i,j))
							addRepulsion(i,j);
				}
			}
        }
        //for number iteration
            //for each poin
            //  for each segment
            //      if check collision(point, segment)
            //          apply impulse to velocity
    }

	void computeSolidForces(VECTOR<float> & xForces, VECTOR<float> & yForces)
	{
		VECTOR<VECTOR_2D<float> > * elasticForces = vElasticForce();
		VECTOR<VECTOR_2D<float> > * dampingForces = vDampForces();

		//now add all dem forces together
		for(int i = 0; i < steps; i++)
		{
			VECTOR_2D<float> force = (*elasticForces)(i) + (*dampingForces)(i) + mG;
			xForces(i) = force.x();
			yForces(i) = force.y();
		}

		

		delete elasticForces;
		delete dampingForces;
	}


	
	void evaluateFE()
	{
		//old stuff
		mPtsOld = mPts;
		mVelsOld = mVels;

		//set endpoints
        
		//mPts[0] = VECTOR_2D<float>(sin(PI*time),0);
		//mPts[steps-1] = VECTOR_2D<float>(1-sin(PI*time),0);
		//mVels[0] = VECTOR_2D<float>(PI*cos(PI*time),0);
		//mVels[steps-1] = VECTOR_2D<float>(-PI*cos(PI*time),0);
        

		SPARSE_MATRIX<float> * massMatrix = vLumpedMassMatrix();
		VECTOR<VECTOR_2D<float> > * elasticForces = vElasticForce();
		VECTOR<VECTOR_2D<float> > * dampingForces = vDampForces();;
		//elasticForces->Print();


		//calculate not endpoints
		for(int i = 1; i < steps; i ++)
		{	
			mPts[i] = mPtsOld[i] + dt * mVelsOld[i];
			mVels[i] = mVelsOld[i] + dt *  (1/(*massMatrix)(i,i)) *  ( (*elasticForces)(i)  + (*dampingForces)(i) + mG);
		}

		delete dampingForces;
		delete massMatrix;
		delete elasticForces;
	}
	
	void evaluateFluid()
	{
		mPtsOld = mPts;
		mVelsOld = mVels;
        //mPts[0] = VECTOR_2D<float>(0.5*sin(PI*time*3),0);
        //mPts[steps-1] = VECTOR_2D<float>(1-0.5*sin(PI*time*3),0);
		
		//forces to pass into incompressible flow
		VECTOR<float> xForces(steps);
		VECTOR<float> yForces(steps);
		computeSolidForces(xForces,yForces);
		//add initial condition forces
		//desired position
		VECTOR_2D<float> desired(0.5+0.25*sin(-2*PI*time*100-PI/2.),0.25*cos(-2*PI*time*100-PI/2)+ 0.5);
		//VECTOR_2D<float> desired(0.5+0.25*sin(2*PI*time*100-PI/2.),0.8);
		//add force to desired position
		xForces(0) = (desired-mPts(0)).x() * 50000;
		yForces(0) = (desired-mPts(0)).y() * 50000;

		for(int i = 0; i < steps; i++)
			mCurrentForces(i) = VECTOR_2D<float>(xForces(i),yForces(i));

		//pass into our solid solver
		fluidSolver.evaluateSolid(mPts,mVels,xForces,yForces,DS());
		computeCollisionsSlow();
		for(int i = 0; i < steps; i ++)
			mPts[i] = mPtsOld[i] + dt * mVels[i];
	}

	void evaluateNEWMARK()
	{

		//old stuff
		mPtsOld = mPts;
		mVelsOld = mVels;

		//set endpoints
		
		//mPts[0] = VECTOR_2D<float>(sin(PI*time),0);
		//mPts[0] = VECTOR_2D<float>(cos(PI*time*3)*0.2,sin(PI*time*8)*0.1);
        mPts[0] = VECTOR_2D<float>(0.5*sin(PI*time*3),0);
        mPts[steps-1] = VECTOR_2D<float>(1-0.5*sin(PI*time*3),0);
		//techincally you should put this after you calculate the velocities, oh well
		//mVels[0] = VECTOR_2D<float>(PI*cos(PI*time),0);
        //mPts[0] = VECTOR_2D<float>(0,0);
        //mVels[0] = VECTOR_2D<float>(0,0);
		//mVels[steps-1] = VECTOR_2D<float>(0,0);
		
        
		//compute stuff
		SPARSE_MATRIX<float> * massMatrix = vMassMatrix();
		VECTOR<float > * elasticForces = vfElasticForce();
		VECTOR<float > * gravityForces = convertStdVectorVector2dToVector(mGravityForces);
		VECTOR<float > * velocities = convertStdVectorVector2dToVector(mVelsOld);
		VECTOR<float > massVelocity(steps*2);
		(*massMatrix).Multiply(*velocities,massVelocity);
		SPARSE_MATRIX<float> * dampMat = vDampMat();

		//formuate (M-K)vNEW = dt*Fel + M*vOLD + dt*Gravity
		VECTOR<VECTOR_2D<float> > * newVels = convertVectorToVectorVector2d( conjugateGradient((*elasticForces)*dt + massVelocity + (*gravityForces)*dt,(*massMatrix)-(*dampMat)) );
		mVels = (*newVels);
	
		for(int i = 1; i < steps-1; i ++)
			mPts[i] = mPtsOld[i] + dt * mVels[i];

		computeCollisionsSlow();

		//no licks
		delete dampMat;
		delete gravityForces;
		delete massMatrix;
		delete elasticForces;
		delete velocities;
		delete newVels;


		
	}

    float tick()
    {
		return time += dt;
    }
};



class P3man
{
	H1PROBLEM3 mProb;
	Vdraw mDraw;
	V3Draw mDraw3D;
public:
    P3man():mProb(1/100000.,20)
	{
    }
	void printPoints()
	{
		for(int i = 0; i < mProb.mPts.size(); i++)
		{
			cout << mProb.mPts[i].x() << " " << mProb.mPts[i].y() << endl;
		}
	}
    void draw()
    {
		mProb.evaluateFluid();
		mDraw3D.drawPoints<VECTOR<VECTOR_2D<float> > >(mProb.fluidSolver.mParticles);
		//mProb.evaluateNEWMARK();
		//mProb.evaluateFE();
		//mDraw.setBoundariesFromPoints<VECTOR<VECTOR_2D<float> > >( mProb.mPts, true);
		mDraw.drawPoints<VECTOR<VECTOR_2D<float> > >(mProb.mPts);
        //mDraw.drawLines(mProb.mPts,mProb.mCurrentForces);
        //mDraw.drawLines(mProb.mPts,mProb.mVels,1);
		//printPoints();
    }
    void tick()
    {
		mProb.tick();
		//cout << "currently at time: " << mProb.tick() << endl;
    }
};
