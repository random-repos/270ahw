#pragma once

#include <cassert>
#include <iostream>
#include <string>
#include <cstring>
#include <stack>
#include "float.h"

namespace ALGEBRA{

class INDEX{
	int i,n;
public:
	INDEX(){i=0;n=0;}
	INDEX(int ai, int an):i(ai),n(an){}
	int Index_Periodic(){while(i < 0) i+=n; return i%n;}
};
class INDEX_2D{
	/* This assumes that we have data at 0 <= i < n, 0 <=j< m. This then maps dofs at these coords to the integer 0 <= i*n+j < m*n
	 */
	
	int i,j,m,n;
	
public:
	INDEX_2D(){i=0;j=0;m=0;n=0;}
	INDEX_2D(const int i_input){i=i_input;j=0;m=0;n=0;}
	INDEX_2D(const int i_input,const int j_input,const int m_input){i=i_input,j=j_input;m=m_input;n=m_input;}
	INDEX_2D(const int i_input,const int j_input,const int m_input,const int n_input){
		i=i_input;
		j=j_input;
		m=m_input;
		n=n_input;}
	/*
	INDEX_2D(INDEX_2D& input){
		i=input.i;
		j=input.j;
		m=input.m;
		n=input.n;
	}*/
	INDEX_2D(const INDEX_2D& input){
		i=input.i;
		j=input.j;
		m=input.m;
		n=input.n;
	}

	INDEX_2D& operator=(const INDEX_2D& index){
		i=index.i;
		j=index.j;
		m=index.m;
		n=index.n;
		return *this;
	}
	
	bool Is_Valid(){
		bool valid=true;
		if(i<0 || i>=m) valid=false;
		if(j<0 || j>=n) valid=false;
		return valid;
	}
	
	static int Index(const int i,const int j,const int m,const int n){
		//assert(j*m+i>=0 && j*m+i<m*n);
		//return j*m+i;
		assert(i*n+j>=0 && i*n+j<m*n);
		return i*n+j;
	}
	
	int& I(){return i;}
	int& J(){return j;}
	int& M(){return m;}
	int& N(){return n;}
	
	int I() const {return i;}
	int J() const {return j;}
	int M() const {return m;}
	int N() const {return n;}
	
	void Print(){
		std::cout<<"Index 2D = {" << i << " , " << j << " , " << m << " , " << n << "}" << std::endl;}
	
/*	int Index_X_MAC(){
		assert(i>=0 && i<m+1 && j>=0 && j<n);		
		return i+(m+1)*j;}
	
	int Index_Y_MAC(){
		assert(i>=0 && i<m && j>=0 && j<n+1);
		return i+m*j;}
	
	int Index_Non_Periodic(){
		assert(i>=0 && i <=m);
		assert(j>=0 && j <=n);
		return j*m+i;
	}
 */
	
	int Index_Periodic(){
		int i_periodic=i_Periodic();
		int j_periodic=j_Periodic();
		
		assert(m==n);
		
		assert(j_periodic*m+i_periodic>=0 && j_periodic*m+i_periodic<m*m);
		return j_periodic*m+i_periodic;}
	
	int Index(){
		//assert(j*m+i>=0 && j*m+i<m*n);
		//return j*m+i;
		assert(i*n+j>=0 && i*n+j<m*n);
		return i*n+j;
	}
	
	int i_Periodic() const {
		assert(m==n);
		int i_periodic;
		if(i<0)
			i_periodic=((((-i)/m)+1)*m+i)%m;
		else
			i_periodic=i%m;
		assert(i_periodic>=0 && i_periodic<m);
		return i_periodic;}
	
	int j_Periodic() const {
		int j_periodic;
		if(j<0)
			j_periodic=((((-j)/m)+1)*m+j)%m;
		else
			j_periodic=j%m;
		assert(j_periodic>=0 && j_periodic<m);
		return j_periodic;}
};

class INDEX_3D{
	/* This assumes that we have data at 0 <= i < n, 0 <= j < m, 0 <= k < mn. This then maps dofs at these coords to the integer 0 <= j*m + i < m*n
	*/
	int i,j,k,m,n,mn;
			
public:
	INDEX_3D(){i=0;j=0;k=0;m=0;n=0;mn=0;}
	INDEX_3D(const int i_input){i=i_input;j=0;k=0;m=0;n=0;mn=0;}
	INDEX_3D(const int i_input,const int j_input,const int k_input,const int m_input,const int n_input,const int mn_input){
		i=i_input;
		j=j_input;
		k=k_input;
		m=m_input;
		n=n_input;
		mn=mn_input;
	}/*
	INDEX_3D(INDEX_3D& input){
		i=input.i;
		j=input.j;
		k=input.k;
		m=input.m;
		n=input.n;
		mn=input.mn;
	}*/
	INDEX_3D(const INDEX_3D& input){
		i=input.i;
		j=input.j;
		k=input.k;
		m=input.m;
		n=input.n;
		mn=input.mn;
	}
			
	INDEX_3D& operator=(const INDEX_3D& index){
		i=index.i;
		j=index.j;
		k=index.k;
		m=index.m;
		n=index.n;
		mn=index.mn;
		return *this;
	}
			
	static int Index(const int i,const int j,const int k,const int m,const int n,const int mn){
		assert(i*mn*n+j*mn+k>=0 && i*mn*n+j*mn+k<m*n*mn);
		return i*mn*n+j*mn+k;
	}
			
	int& I(){return i;}
	int& J(){return j;}
	int& K(){return k;}
	int& M(){return m;}
	int& N(){return n;}
	int& MN(){return mn;}
			
	int I() const {return i;}
	int J() const {return j;}
	int K() const {return k;}
	int M() const {return m;}
	int N() const {return n;}
	int MN() const {return mn;}
			
	void Print(){
		std::cout<<"Index 3D = {" << i << " , " << j << " , " << k << " , " << m << " , " << n << " , " << mn << "}" << std::endl;}
			
	int Index(){
		assert(i*mn*n+j*mn+k>=0 && i*mn*n+j*mn+k<m*n*mn);
		return i*mn*n+j*mn+k;
	}
};	

template<class T>
class LIST{
	//This is a dynamically resizable VECTOR. It just keeps a buffer and then resizes if more info than available in the buffer is requested
	
	int n,n_extended;//n is the apparent size of the array and n_extended is the actual size of the array
	int buffer_size;//Whenever the array needs to be resized becasue we have passed n_extended, we add the buffer_size more entries than would be minimally necessary
	T* values;
public:
	LIST():n(0),buffer_size(5){
		n_extended=n+buffer_size;
		values=new T[n_extended];
		for(int i=0;i<n_extended;i++) values[i]=T();}
	
	LIST(const int n_input):n(n_input),buffer_size(5){
		n_extended=n+buffer_size;
		values=new T[n_extended];
		for(int i=0;i<n_extended;i++) values[i]=T();}	
	LIST(const LIST & o)
	{
		values = new T[o.n_extended];
		n = o.n;
		n_extended = o.n_extended;
		buffer_size = o.buffer_size;
		for(int i = 0; i < n; i++)
		{
			values[i] = o.values[i];
		}
	}
	
	int Size(){return n;}
	
	T& operator()(const int i){assert(i<n); return values[i];}
	
	void Resize(const int n_input){
		if(n_input>n_extended){
			delete values;
			n_extended=n_input+buffer_size;
			values=new T[n_extended];
			for(int i=0;i<n_extended;i++) values[i]=T();}
		else{
			n=n_input;}}
	
	void Append_Element(const T& entry){
		if(n<n_extended){
			n=n+1;
			values[n-1]=entry;}
		else{
			//delete values;
			n=n+1;
			n_extended=n+buffer_size;
			T* values_new=new T[n_extended];
			for(int i=0;i<n_extended;i++) values_new[i]=T();
			for(int i=0;i<n-1;i++) values_new[i]=values[i];
			values_new[n-1]=entry;
			delete values;
			values=values_new;}
	}
};


template<class T>
class VECTOR_2D{
    T v1,v2;
public:
	VECTOR_2D(const T input):v1(input),v2(input){}
	VECTOR_2D():v1((T)0),v2((T)0){}
	VECTOR_2D(T v1_input,T v2_input):v1(v1_input),v2(v2_input){}
	VECTOR_2D(const VECTOR_2D<T>& v_input):v1(v_input.v1),v2(v_input.v2){}
		
	VECTOR_2D<T>& operator=(const VECTOR_2D<T>& input){v1=input.v1;v2=input.v2;return *this;}

    //PETER DID THIS
	VECTOR_2D<T> operator-(const VECTOR_2D<T>& input) const {
        return VECTOR_2D<T>(v1-input.v1,v2-input.v2);
    }
	VECTOR_2D<T> operator+(const VECTOR_2D<T>& input) const {
        return VECTOR_2D<T>(v1+input.v1, v2+input.v2);
    }
	VECTOR_2D<T> operator*(const T scale) const {
        return VECTOR_2D<T>(v1*scale,v2*scale);
    }
	VECTOR_2D<T> operator-() const {
        return (*this)*(-1);
    }
	VECTOR_2D<T> operator+=(const VECTOR_2D<T>& input) {
		v1 += input.v1;
		v2 += input.v2;
		return *this;
	}
	VECTOR_2D<T> operator-=(const VECTOR_2D<T>& input) {
		v1 -= input.v1;
		v2 -= input.v2;
		return *this;
	}
    //END PETER DID THIS
	
	T operator()(const int component)const {
		assert(component==0 || component==1);
		return component?v2:v1;}
	
	VECTOR_2D<T> Right_Handed_Perp_Vector(){return VECTOR_2D<T>(-v2,v1);}
	
	static T Dot_Product(const VECTOR_2D<T>& u,const VECTOR_2D<T>& v){return u.Dot(v);}
	static T Signed_Triangle_Area(const VECTOR_2D<T>& u,const VECTOR_2D<T>& v){return (T).5*(u.x_copy()*v.y_copy()-v.x_copy()*u.y_copy());}
	static VECTOR_2D<T> ei(const int i){
		assert(i>=0 && i<2);
		if(i==0)
			return VECTOR_2D<T>((T)1,0);
		else
			return VECTOR_2D<T>(0,(T)1);}
	
	T& x() {return v1;} 
	T& y() {return v2;}
	const T& x() const {return v1;} 
	const T& y() const {return v2;}
	
	T x_copy()const{return v1;}
	T y_copy()const{return v2;}
	
	T Dot(const VECTOR_2D<T>& v)const {return v.v1*v1+v.v2*v2;}
	
	T Magnitude(){return sqrt(v1*v1+v2*v2);}
	
	void Normalize(){
		T n=sqrt(v1*v1+v2*v2);
		if(n!=(T)0){
			v1=v1/n;
			v2=v2/n;}}
};


//PETER DID THIS
static VECTOR_2D<double> operator*(const double scale,const VECTOR_2D<double>& input){
    return input*scale;
}
static VECTOR_2D<float> operator*(const float scale,const VECTOR_2D<float>& input){
    return input*scale;
}
static float crossProduct(VECTOR_2D<float> v1,VECTOR_2D<float> v2)
{
	return v1.x()*v2.y() - v1.y()*v2.y();
}
//PETER DID THIS

template<class T>
class VECTOR{
    int n;
    T* values;
public:
    VECTOR():n(1) {
        values=new T[n];
		for(int i=0;i<=n-1;i++) values[i]=T();}

    VECTOR(const int n_input):n(n_input) {
        values=new T[n];
		for(int i=0;i<=n-1;i++) values[i]=T();}

    //PETER WROTE THIS
    VECTOR(const VECTOR<T> & v)
    {
        n = v.n;
        values = new T[n];
        for(int i = 0; i < n; i++)
            values[i] = v.values[i];
    }
	//PETER WROTE THESE
	//using these functions is probably rather inefficient
	VECTOR<T> operator*(const float v) const
	{
		VECTOR<T> r(n);
		for(int i = 0; i < n; i++)
			r(i) = (*this)(i)*v;
		return r;
	}
	VECTOR<T> operator+(const VECTOR<T> & v)
	{
		VECTOR<T> r(v.n);
		for(int i = 0; i < n; i++)
			r(i) = (*this)(i) + v(i);
		return r;
	}
	//END PETER WROTE THESE

	//PETER WROTE THESE (efficiently)
	//these are implemented below
	
	VECTOR<T>& operator*=(const float  & v)
	{
		for(int i = 0; i < n; i++)
			(*this)(i) *= v;
		return *this;
	}
	VECTOR<T>& operator+=(const VECTOR<T> & v)
	{
		for(int i = 0; i < n; i++)
			(*this)(i) += v(i);
		return (*this);
	}
	//END PETER WROTE THESE

    ~VECTOR() {delete[] values;}

    bool Resize(const int n_new){
        if (n_new==n) return true;
        assert(n_new>0);
        delete[] values;
        values=new T[n_new];
        n=n_new;
        for(int i=0;i<=n-1;i++) values[i]=T();
        return (values!=NULL);}
	
	VECTOR<T>& operator=(const VECTOR<T>& input){
		assert(input.Size()==this->Size());
		for(int i=0;i<n;i++) values[i]=input.values[i];
		return *this;}
	
	T Dot(VECTOR<T>& x){
		T result=(T)0;
		for(int i=0;i<n;i++) result+=values[i]*x(i);
		return result;
	}
	
	T Min(){
		T min=FLT_MAX;
		for(int i=0;i<n;i++) if(values[i]<min) min=values[i];
		return min;
	}
	
	T Max(){
		T max=-FLT_MAX;
		for(int i=0;i<n;i++) if(values[i]>max) max=values[i];
		return max;
	}
	
    //PETER WROTE THIS
    T& operator()(const int i) {
        return values[i];
    }
	T& operator[](const int i) {
        return values[i];
    }
	T operator()(const int i) const {
        return values[i];
    }
	T operator[](const int i) const {
        return values[i];
    }
	/*
    void operator+=(VECTOR<T>& x) {
        for(int i = 0; i < n; i++)
            values[i] += x(i);
    }*/
	void operator-=(const VECTOR<T>& x) const {
        for(int i = 0; i < n; i++)
            values[i] -= x(i);
    }
    //END PETER WROTE THIS
	
	void Set_To_Zero(){for(int i=0;i<n;i++) values[i]=0;}
	
	T L_inf(){
		T max_norm=(T)0;
		for(int i=0;i<n;i++) if(fabs(values[i])>max_norm) max_norm=fabs(values[i]);
		return max_norm;}
	
	T Sum(){T sum=(T)0;for(int i=0;i<n;i++) sum+=values[i];return sum;}
	
	int Size() const {return n;}
	int size() const {return Size();}
	
	void Enforce_Zero_Sum(){
		T sum=(T)0;
		for(int i=0;i<n;i++) sum+=values[i];
		for(int i=0;i<n;i++) values[i]-=sum/((T)n);}
	
	void Write_DAT_File(std::string file){
		FILE* fpointer;
		fpointer=fopen(file.c_str(),"w");
		for(int i=0;i<n;i++)
			fprintf(fpointer,"%g\n",(double)values[i]);
		fclose(fpointer);}
	
	void Print(){
		std::cout << "Vector =";
		for(int i=0;i<n;i++) std::cout << " " << values[i] << " , ";
		std::cout << "." << std::endl;}
};

template <class T>
std::ostream& operator<<(std::ostream & os, const VECTOR<T>& v){
    os<<"[";for(int i=0;i<v.Size();i++) os<<v(i)<<" ";os<<"]";
    return os;
}
template <class T>
std::ostream& operator<<(std::ostream & os, const VECTOR_2D<T>& v){
    os<<"["<<v.x()<<" "<<v.y()<<"]";
    return os;
}

template<class T>
class SPARSE_ROW{
    const int n;
    int size;
    int* indices;
    T* values;
public:
    SPARSE_ROW(const int n_input):n(n_input),size(0),indices(0),values(0) {}

    //PETER WROTE THIS FUNCTION
    SPARSE_ROW(const SPARSE_ROW<T> & v):n(v.n),size(v.size)
    {
        indices = new int[size];
        memcpy(indices,v.indices,sizeof(int)*size);
        values = new T[size];
        for(int i = 0; i < size; i++)
            values[i] = v.values[i];
    }
	//PETER WROTE THIS FUNCTION
	SPARSE_ROW<T>& operator=(const SPARSE_ROW<T> & v)
	{
		assert(n == v.n);
		delete [] indices;
		delete [] values;

		size = v.size;
		indices = new int[size];
        memcpy(indices,v.indices,sizeof(int)*size);
        values = new T[size];
        for(int i = 0; i < size; i++)
            values[i] = v.values[i];

		return *this;
	}
	int N() {return n;}
    ~SPARSE_ROW() {delete[] indices;delete[] values;}
	
	void Zero_Out_Without_Changing_Sparsity(){for(int i=0;i<size;i++) indices[i]=(T)0;}
	
	bool Is_Non_Zero(const int index){for(int i=0;i<size;i++) if(indices[i]==index) return true;return false;}

    T& operator()(const int i){
        assert(0<=i && i<=n-1);
        for(int j=0;j<=size-1;j++) if(indices[j]==i) return values[j];
		assert(false);
		return values[0];}
	
	void Normalize_Row_Sum(){
		T sum=(T)0;
		for(int i=0;i<=size-1;i++) sum+=values[i];
		assert(sum!=0);
		for(int i=0;i<=size-1;i++) values[i]=values[i]/sum;
	}
	
	void Scale(T scale){
		for(int i=0;i<=size-1;i++) values[i]*=scale;}
	
	void Fill_Vector(VECTOR<T>& v){
		assert(v.Size()==n);
		for(int i=0;i<n;i++) v(i)=(T)0;
		for(int i=0;i<size;i++) v(indices[i])=values[i];}
	
	void Print(){
		std::cout << "Sparse Row =";
		for(int i=0;i<n;i++){
			bool found=false;
			for(int j=0;j<=size-1;j++){
				if(indices[j]==i){
					std::cout << " " << values[j] << " , ";
					found=true;}}
			if(!found)
				std::cout << " " << 0 << " , ";}
		std::cout << "." << std::endl;}

	int Number_Nonzero(){return size;}
	
	bool Value_Exists_At_Entry(const int index){
		for(int i=0;i<size;i++) 
			if(indices[i]==index) return true;
		return false;
	}
	
	int Index(const int i_hat){assert(i_hat<size);return indices[i_hat];}
	
	T Value_At_Sparse_Index(const int i_hat){assert(i_hat<size);return values[i_hat];}
	
    T Dot_Product( const VECTOR<T>& v) const {
		assert(v.Size()==n);
        T result=0;
		for(int i=0;i<=size-1;i++) 
		{
			result+=values[i]*v(indices[i]);
		}
        return result;}

    void Add_Entry(const int index,const T value){
		bool found=false;int entry=0;
		for(int i=0;i<size;i++) if(indices[i]==index){found=true;entry=i;}
		if(found){
			int non_zero_index=indices[entry];
			values[non_zero_index]=value;
			return;}
        size++;int* new_indices=new int[size];T* new_values=new T[size];
        for(int i=0;i<=size-2;i++){
            new_indices[i]=indices[i];new_values[i]=values[i];}
        new_indices[size-1]=index;delete[] indices;indices=new_indices;
        new_values[size-1]=value;delete[] values;values=new_values;}

};


template<class T>
class MATRIX_MXN{
	const int m,n;
	VECTOR<T>** rows;
public:
	MATRIX_MXN(const int m_input,const int n_input):m(m_input),n(n_input){
		rows=new VECTOR<T>*[m];
        for(int i=0;i<=m-1;i++) rows[i]=new VECTOR<T>(n);}

    //PETER WROTE THIS FUNCTION
    MATRIX_MXN(const MATRIX_MXN<T> & v):m(v.m),n(v.n)
    {
		rows=new VECTOR<T>*[m];
		for(int i=0;i<=m-1;i++) rows[i]=new VECTOR<T>(n);
        for(int i = 0; i < m; i++)
            *rows[i] = *v.rows[i];
    }
	
	int M() const {return m;}
	int N() const {return n;}
	
	void Set_To_Zero(){
		for(int i=0;i<m;i++)for(int j=0;j<n;j++) (*this)(i,j)=(T)0;
	}

	//PETER DI THIS ONE
	void setToValue(const T & value)
	{
		for(int i=0;i<m;i++)for(int j=0;j<n;j++) (*this)(i,j)=value;
	}
	//PETER DI THIS ONE
	void setScalarMatrix(const T & value)
	{
		assert(m == n);
		for(int i=0;i<m;i++) (*this)(i,i)=value;
	}
	
	T& operator()(const int i,const int j){
        assert(0<=i && i<=m-1);return (*rows[i])(j);}
    
	const T& operator() (const int i,const int j) const {
        assert(0<=i && i<=m-1);return (*rows[i])(j);}
    

	//PETETR DID THIS
	MATRIX_MXN<T>& operator+=(MATRIX_MXN<T> o)
	{
		assert(m == o.m && n == o.n);
		for(int i = 0; i < m; i++)
			for(int j = 0; j < n; j++)
				(*this)(i,j) += o(i,j);
		return *this;
	}
	MATRIX_MXN<T>& operator*=(T v)
	{
		for(int i = 0; i < m; i++)
			for(int j = 0; j < n; j++)
				(*this)(i,j) *= v;
		return *this;
	}
	//END PETER DID THIS
	
	void Transpose(MATRIX_MXN<T>& result){
		assert(result.M()==n && result.N()==m);
		for(int i=0;i<result.M();i++){
			for(int j=0;j<result.N();j++){
				result(i,j)=(*this)(j,i);}}
	}
	
    void Print(){
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++)
			    std::cout<<(*this)(i,j)<<" ";
			std::cout<<std::endl;}
    }
	//PETER DID THIS
	/*
	SPARSE_MATRIX<T> lump()
	{
		SPARSE_MATRIX<T> r(m,n);
		//for each collumn
		for(int i = 0; i < n; i++)
		{
			r.Row(i).Add_Entry(i,0);
			for( int j = 0; j < m; j++)
			{
				r(i,i) += (*this)(i,j);
			}
		}
		return r;
}*/
    
};
	
static void Multiply(MATRIX_MXN<double>& A, MATRIX_MXN<double>& B, MATRIX_MXN<double>& result){
	assert(A.N()==B.M());
	assert(result.M()==A.M());
	assert(result.N()==B.N());
	for(int i=0;i<result.M();i++){
		for(int j=0;j<result.N();j++){
			result(i,j)=(double)0;
			for(int k=0;k<A.N();k++){
				result(i,j)+=A(i,k)*B(k,j);}}}
}	

static void Multiply(MATRIX_MXN<float>& A, MATRIX_MXN<float>& B, MATRIX_MXN<float>& result){
	assert(A.N()==B.M());
	assert(result.M()==A.M());
	assert(result.N()==B.N());
	for(int i=0;i<result.M();i++){
		for(int j=0;j<result.N();j++){
			result(i,j)=(float)0;
			for(int k=0;k<A.N();k++){
				result(i,j)+=A(i,k)*B(k,j);}}}
}	


template<class T>
class SPARSE_MATRIX{
    const int m,n;
    SPARSE_ROW<T>** rows;
public:
    SPARSE_MATRIX(const int m_input,const int n_input):m(m_input),n(n_input){
        rows=new SPARSE_ROW<T>*[m];
        for(int i=0;i<=m-1;i++) rows[i]=new SPARSE_ROW<T>(n);}

    SPARSE_MATRIX(const SPARSE_MATRIX<T> & v):m(v.m),n(v.n)
    {
		rows=new SPARSE_ROW<T>*[m];
		for(int i=0;i<=m-1;i++) rows[i]=new SPARSE_ROW<T>(n);
        for(int i = 0; i < m; i++)
            *rows[i] = *v.rows[i];
    }
	//peter wrote this
	SPARSE_MATRIX<T> & operator=(const SPARSE_MATRIX<T> & v)
	{
		assert(m == v.m && n == v.n);
		//clean up
		for(int i=0;i<=m-1;i++) delete rows[i];delete[] rows;
		//recreate
		rows=new SPARSE_ROW<T>*[m];
		for(int i=0;i<=m-1;i++) rows[i]=new SPARSE_ROW<T>(n);
        for(int i = 0; i < m; i++)
            *rows[i] = *v.rows[i];
		return *this;
	}

    ~SPARSE_MATRIX() {for(int i=0;i<=m-1;i++) delete rows[i];delete[] rows;}

    void Zero_Out_Without_Changing_Sparsity(){
		for(int i=0;i<m;i++) rows[i]->Zero_Out_Without_Changing_Sparsity();}
	
	T& operator()(const int i,const int j){
        assert(0<=i && i<=m-1);return (*rows[i])(j);}

	T& operator()(const int i,const int j) const{
        assert(0<=i && i<=m-1);return (*rows[i])(j);}

	//PETER WROTE THIS ONE
	bool hasEntry(const int i, const int j) const{
		return (*rows[i]).Value_Exists_At_Entry(j);
	}

	//PETER DID THIS ONE
	T& CreateAndGetAt(const int i,const int j)
	{
        assert(0<=i && i<=m-1);
		if(!rows[i]->Value_Exists_At_Entry(j))
			rows[i]->Add_Entry(j,0);
		return (*rows[i])(j);
	}

	//PETER WROTE THIS ONE
	SPARSE_MATRIX<T> operator-(const SPARSE_MATRIX<T> & mat) const
	{
		assert(mat.N() == n && mat.M() == m);
		//inefficiency cuz it copies this but whatever
		SPARSE_MATRIX<T> r(m,n);
		for(int i = 0; i < m; i++)
			for(int j = 0; j < n; j++)
			{
				T left = hasEntry(i,j) ? (*this)(i,j) : (T)0;
				T right = mat.hasEntry(i,j) ? mat(i,j) : (T)0;
				r.CreateAndGetAt(i,j) = left-right;
			}
		return r;
	}

	

	
	void Column(const int j,VECTOR<T>& c){
		assert(j>=0 && j<n && c.Size()==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ai=Row(i);
			if(Ai.Is_Non_Zero(j)) c(i)=Ai(j);
			else c(i)=(T)0;}}
	
	void Normalize_Row_Sums(){
		for(int i=0;i<m;i++) Row(i).Normalize_Row_Sum();
	}
	
	void Right_Multiply(SPARSE_MATRIX<T>& B,SPARSE_MATRIX<T>&AB){
		assert(n==B.M());
		assert(AB.N()==B.N());
		assert(AB.M()==m);
		VECTOR<T> column(B.M());
		for(int i=0;i<AB.M();i++){
			for(int j=0;j<B.N();j++){
				B.Column(j,column);
				SPARSE_ROW<T>& Ai=Row(i);
				SPARSE_ROW<T>& ABi=AB.Row(i);
				//ABi(j)=Ai.Dot_Product(column);
				//PETER DID THIS
				ABi.Add_Entry(j,Ai.Dot_Product(column));
			}}} 
	
	void Scale_Rows(T scale){
		for(int i=0;i<=m-1;i++) rows[i]->Scale(scale);}
	
	void Print(){
		std::cout << "Sparse Matrix = " << std::endl;
		for(int i=0;i<m;i++)
			Row(i).Print();}

    SPARSE_ROW<T>& Row(const int i) const {
        assert(0<=i && i<=m-1);return *rows[i];}
	
	void Residual(VECTOR<T>& rhs,VECTOR<T>& x,VECTOR<T>& r)
	{
		for(int i=0;i<m;i++){
		SPARSE_ROW<T>& Ai=Row(i);
		r(i)=rhs(i)-Ai.Dot_Product(x);}}
	
	void Multiply(const VECTOR<T>& x,VECTOR<T>& b) const {//as in b=Ax
		assert(b.Size()==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ai=Row(i);
			b(i)=Ai.Dot_Product(x);}}
	
	T A_Norm_Squared(const VECTOR<T>& x){
		T result;
		assert(x.Size()==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ai=Row(i);
			result+=Ai.Dot_Product(x)*x(i);}
		return result;
	}
	
	int M() const {return m;}
	
	int N() const {return n;}
	
	/*
	void Multiply(const VECTOR<T>& x,VECTOR<T>& b)
	{for(int i=0;i<m;i++){
		SPARSE_ROW<T>& ri=Row(i);
		b(i)=ri.Dot_Product(x);}}*/
	
	void Transpose(SPARSE_MATRIX<T>& transpose)
	{
		assert(transpose.m==n && transpose.n==m);
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& Ri=Row(i);
			for(int j_hat=0;j_hat<Ri.Number_Nonzero();j_hat++){
				int j=Ri.Index(j_hat);
				SPARSE_ROW<T>& Pj=transpose.Row(j);
				Pj.Add_Entry(i,Ri.Value_At_Sparse_Index(j_hat));}}
	}
	
	void Write_DAT_File(std::string file){
		FILE* fpointer;
		fpointer=fopen(file.c_str(),"w");
		for(int i=0;i<m;i++){
			SPARSE_ROW<T>& row=Row(i);
			for(int j=0;j<n;j++){
				bool found=false;
				if(row.Value_Exists_At_Entry(j)){
						fprintf(fpointer,"%g ",(*this)(i,j));
					found=true;}
				if(!found) fprintf(fpointer,"%g ",(T)0);}
			fprintf(fpointer,"\n");}
		fclose(fpointer);
	}

   //PETERDID THIS
    //sets submatrix starting at (inclusive) row i, col j, 
    void SetUsingMatrix(int ai, int aj, const MATRIX_MXN<T> & mat)
    {
        for(int i = 0; i <  mat.M(); i++)
            for(int j = 0; j <  mat.N(); j++) 
                (*this).CreateAndGetAt(i+ai, j+aj) = mat(i,j);

    }
};
	


template<class T>
class CONJUGATE_GRADIENT{
	SPARSE_MATRIX<T>& A;
	VECTOR<T>& x;
	VECTOR<T>& b;
	VECTOR<T> r,p,q;
	VECTOR<int>* dirichlet_dofs;
	int max_iterations;
	T tolerance;
	
public:
	CONJUGATE_GRADIENT(SPARSE_MATRIX<T>& A_input,VECTOR<T>& x_input,VECTOR<T>& b_input,const int max_it):A(A_input),x(x_input),b(b_input),r(x_input.Size()),max_iterations(max_it),
	p(x_input.Size()),q(x_input.Size()),dirichlet_dofs(0){
		Set_Tolerance((T)1e-6);
	}
	
	void Set_Tolerance(const T tol_input){tolerance=tol_input;}
	
	void Set_Dirichlet_Dofs(VECTOR<int>& dirichlet){
		dirichlet_dofs=&dirichlet;
	}
	
	void Zero_Dirichlet_Residual(){
		if(dirichlet_dofs){
			for(int i=0;i<dirichlet_dofs->Size();i++) r((*dirichlet_dofs)(i))=(T)0;}
	}
	
	int Solve(const bool verbose=false){
		A.Residual(b,x,r);
		Zero_Dirichlet_Residual();
		if(r.L_inf()<tolerance) return 0;
		p=r;
		A.Multiply(p,q);
		T r_dot_r=r.Dot(r);
		T alpha=r_dot_r/p.Dot(q);
		
		for(int it=0;it<max_iterations;it++){
			
			for(int i=0;i<r.Size();i++){
				x(i)+=alpha*p(i);
				r(i)-=alpha*q(i);}
			
			Zero_Dirichlet_Residual();
			
			if(verbose) std::cout << "Residual at iteration it+1 = " << r.L_inf() << std::endl;
			if(r.L_inf()<tolerance) return it+1;
			
			T r_dot_r_new=r.Dot(r);
			T beta=r_dot_r_new/r_dot_r;
			r_dot_r=r_dot_r_new;
			
			for(int i=0;i<p.Size();i++) p(i)=beta*p(i)+r(i);
			
			A.Multiply(p,q);
			alpha=r_dot_r/p.Dot(q);}
		
		return max_iterations;
	}
	
};
template<class T>
class BOX
{
	T xmax, xmin, ymax, ymin;
public:
	BOX(T l, T r, T u, T d):xmax(r),xmin(l),ymax(u),ymin(d)
	{}
	bool intersects(const VECTOR_2D<T>& point)
	{
		return xmin <= point.x() && point.x() <= xmax && ymin <= point.y() && point.y() <= ymax;
	}
};

template<class T>
bool intersects( const VECTOR_2D<T> & point, const BOX<T> & box)
{
    return box.intersects(point);
}

template<class T>
BOX<T> unionBoxes(const BOX<T> & b1, const BOX<T> & b2)
{
	BOX<T> r;
	r.xmin = min(b1.xmin,b2.xmin);
	r.xmax = max(b1.xmax,b2.xmax);
	r.ymin = min(b1.ymin,b2.ymin);
	r.ymax = max(b1.ymax,b2.ymax);
	return r;
}

template<class T>
class STACK
{
    std::stack<T> data;
public:
    void reset()
    {
        while(!data.empty())
		data.pop();
    }
    bool empty()
    {
        return data.empty();
    }
    void push(T elt)
    {
	    data.push(elt);
    }
    T pop()
    {
        return data.pop();	
    }

};

template<class T>
class AABB_HIERARCHY
{
    int numberOfLeaves, root;
    LIST<int> parents;
    LIST<VECTOR_2D<int> > children;
    LIST<BOX<T> > hierarchy;
    STACK<int> pStack;
public:
	AABB_HIERARCHY()
	{
		//TODO
	}
    LIST<int> INTERSECTION_LIST(const VECTOR_2D<T> & pt)
    {
        pStack.reset();
        LIST<int> inter;
        pStack.push(root);
        while(!pStack.empty())
        {
            int box_index = pStack.pop();
            //if no collisions we are done
            if(!intersects<T>(pt,hierarchy(box_index))) continue;
            //leaf case,we have collision
            if(box_index < numberOfLeaves) inter.Append_Element(box_index);
            else
            {
                int child1 = children(box_index).x;
                int child2 = children(box_index).y;
                if(child1 != -1) pStack.push(child1);
                if(child2 != -1) pStack.push(child2);
            }
        }
        return inter;

    }
    LIST<int> INTERSECTION_LIST(const BOX<T> & box)
    {
	pStack.reset();
        LIST<int> inter;
        pStack.push(root);
        while(!pStack.empty())
        {
            int box_index = pStack.pop();
            //if no collisions we are done
            if(!intersects<T>(box,hierarchy(box_index))) continue;
            //leaf case,we have collision
            if(box_index < numberOfLeaves) inter.Append_Element(box_index);
            else
            {
                int child1 = children(box_index).x;
                int child2 = children(box_index).y;
                if(child1 != -1) pStack.push(child1);
                if(child2 != -1) pStack.push(child2);
            }
        }
        return inter;
    }
    void update_non_leaf_boxes()
    {
        //TODO
    }
};






















}
