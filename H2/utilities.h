#pragma once
#include <iostream>
using namespace std;
#define CLAMP(v,l,h)	((v) < (l) ? (l) : (v) > (h) ? (h) : (v))

class Vcamera
{
	int mW, mH;
	bool init;
	float mPhi,mTheta;
	float mZoom;
public:
	Vcamera()
	{
		mPhi = mTheta = 0;
		mZoom = 1;
		init = false;
	}
	~Vcamera(){}
	void initialize(int aW, int aH)
	{
		//TODO check for stupid values of aW and aH
		init = true;
		mW = aW;
		mH = aH;
	}
	void setRotation(float aPhi, float aTheta, float zoom)
	{
		mPhi = aPhi;
		mTheta = aTheta;
		mZoom = zoom;
	}

	void setCamera()
	{
		if(!init)
			return;
		
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float d = 50;
		float h = mW;
		float w = mH;
		float eyeX 	= w / 2.0;
		float eyeY 	= h / 2.0;
		float fov = 2*atan(eyeY/d);
		float nearDist 	= d / 100.0;
		float farDist 	= d * 100.0;	
		float aspect 			= (float)w/(float)h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov*180/PI, w/h, nearDist, farDist);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(0, 0, d, 0, 0, 0.0, 0.0, 1.0, 0.0);
		
		glScalef(w,h,1);	//flip the y axis and scale sides to 0 to 1
		glScalef(mZoom,mZoom,mZoom);
		glTranslatef(-.5,-.5,0);

		//cout << mPhi << " " << mTheta << endl;
		glRotatef(mPhi,0,1,0);
		glRotatef(mTheta,-1,0,0);
	}
private:
	
};

class V3Draw
{
public:
    V3Draw()
    {
    }
    template<typename C,typename D>
    void drawGrid(const C & base, const D & values, int w, int h,int color = 0)
    {
        assert(w*h == base.size());
        switch(color)
        {
            case 0:
        		glColor3f(0,1,0);
                break;
            case 1:
                glColor3f(0,0,1);
                break;
            default:
                glColor3f(1,1,1);
                break;
        }
        glBegin(GL_LINES);
        for(int i = 0; i < h-1; i++)
            for(int j = 0; j < w - 1; j++)
            {
                glVertex3f(base(i*w + j).x(),base(i*w+j).y(),values(i*w+j));
                glVertex3f(base((i+1)*w + j).x(),base((i+1)*w+j).y(),values((i+1)*w+j));
                glVertex3f(base(i*w + j).x(),base(i*w+j).y(),values(i*w + j));
                glVertex3f(base(i*w + j+1).x(),base(i*w+j+1).y(),values(i*w + j +1));
            }
        glEnd();
    }

	template<typename C>
	void drawPoints(const C &  pts, int color = 0)
	{
        switch(color)
        {
            case 0:
        		glColor3f(0,1,0);
                break;
            case 1:
                glColor3f(0,0,1);
                break;
            default:
                glColor3f(1,1,1);
                break;
        }
		glBegin(GL_POINTS);
        for(int i = 0; i < pts.size(); i++)
        {
			glVertex3f(pts[i].x(),pts[i].y(),0);
        }
        glEnd();
	}

	template<typename C>
	void drawLines(const C &  base, const C & vects, int color = 0)
	{
        switch(color)
        {
            case 0:
        		glColor3f(0,1,0);
                break;
            case 1:
                glColor3f(0,0,1);
                break;
            default:
                glColor3f(1,1,1);
                break;
        }
		glBegin(GL_LINES);
        for(int i = 0; i < base.size(); i++)
        {
			glVertex3f(base[i].x(),base[i].y(),0);
			glVertex3f(vects[i].x()+base[i].x(),vects[i].y()+base[i].y(),0);
        }
        glEnd();
	}
};

class Vdraw
{
    float l,r,u,d;
public:
    Vdraw()
    {
        l = d = 0;
        u = r = 1;
    }
    ~Vdraw()
    {
    }
    void setBoundaries(float al, float ar, float au, float ad)
    {
        l = al;
        r = ar;
        u = au;
        d = ad;
    }

	template<typename T>
    void setBoundariesFromPoints(const T & pts, bool enlarge = false)
    {
        if(pts.size()==0)
        {
            std::cout << "SETTING BOUNDARY FROM EMPTY VECTOR" << std::endl;
            return;
        }
        float maxx, minx, maxy, miny;
        maxx = minx = pts[0].x();
        maxy = miny = pts[0].y();

		if(enlarge)
		{
			maxx = max(maxx,r);
			minx = min(minx,l);
			maxy = max(maxy,u);
			miny = min(miny,d);
		}

        for(int i = 1; i < pts.size(); i++)
        {
            if(pts[i].x() <  minx)
                minx = pts[i].x();
            if(pts[i].x() > maxx)
                maxx = pts[i].x();
            if(pts[i].y() < miny)
                miny = pts[i].y();
            if(pts[i].y() > maxy)
                maxy = pts[i].y();
        }
		l = minx;
		r = maxx;
		u = maxy;
		d = miny;
    }

	template<typename C>
	void setBoundariesFromLines(const C &  pts, const C & vects, bool enlarge = false)
	{
		C r(pts.size());
		for(int i = 0; i < r.size(); i ++)
		{
			r[i] = pts[i] + vects[i];
		}
		setBoundariesFromPoints(r,enlarge);
	}
	template<typename C>
	void drawPoints(const C &  pts, int color = 0)
    {
		 switch(color)
        {
            case 0:
        		glColor3f(1,0,0);
                break;
            case 1:
                glColor3f(0,1,0);
                break;
            default:
                glColor3f(0,0,1);
                break;
        }
        glBegin(GL_LINE_STRIP);
        for(int i = 0; i < pts.size(); i++)
        {
            glVertex3f((-l + pts[i].x())/(r-l),(-d + pts[i].y())/(u-d),0);
        }
        glEnd();
		glDisable(GL_DEPTH_TEST);
		glBegin(GL_POINTS);
		glColor3f(1,1,1);
        for(int i = 0; i < pts.size(); i++)
        {
            glVertex3f((-l + pts[i].x())/(r-l),(-d + pts[i].y())/(u-d),0);
        }
        glEnd();
		glColor3f(1,0,0);
    }
	template<typename C>
	void drawLines(const C &  pts, const C & vects, int color = 0)
	{
        switch(color)
        {
            case 0:
        		glColor3f(0,1,0);
                break;
            case 1:
                glColor3f(0,0,1);
                break;
            default:
                glColor3f(1,1,1);
                break;
        }
		glBegin(GL_LINES);
        for(int i = 0; i < pts.size(); i++)
        {
			glVertex3f((-l + pts[i].x())/(r-l),(-d + pts[i].y())/(u-d),0);
			glVertex3f((-l + pts[i].x()+vects[i].x())/(r-l),(-d + pts[i].y()+vects[i].y())/(u-d),0);
        }
        glEnd();
	}

};


class VISUrotozoom
{
	float mX, mY, mZ, mStep, mZStep;;
	bool u,d,l,r,i,o;
public:
	VISUrotozoom()
	{
		mX = 0;
		mY = 0;
		mZ = 1;
		
		mStep = 1;
		mZStep = 0.01;
		u = d = l = r = i = o = false;
	}
	float getX()
	{
		return mX;
	}
	float getY()
	{
		return mY;
	}
	float getZ()
	{
		return mZ;
	}
	void update()
	{
		if(u)
			mY += mStep;
		else if(d)
			mY -= mStep;

		if(l)
			mX -= mStep;
		else if(r)
			mX += mStep;

		if(i)
			mZ += mZStep;
		else if(o)
			mZ -= mZStep;

	}
	void keyReleased(int key)
	{
		switch(key)
		{
		case UP_ARROW:
			u = false;
			break;
		case DOWN_ARROW:
			d = false;
			break;
		case LEFT_ARROW:
			l = false;
			break;
		case RIGHT_ARROW:
			r = false;
			break;
		case PAGE_UP:
			i = false;
			break;
		case PAGE_DOWN:
			o = false;
			break;
		default:
			break;
		}
	}
	void keyPressed(int key)
	{
		switch(key)
		{
		case UP_ARROW:
			u = true;
			break;
		case DOWN_ARROW:
			d = true;
			break;
		case LEFT_ARROW:
			l = true;
			break;
		case RIGHT_ARROW:
			r = true;
			break;
		case PAGE_UP:
			i = true;
			break;
		case PAGE_DOWN:
			o = true;
			break;
		default:
			break;
		}
	}
private:
};








class Function2D
{
protected:
    float dt, ds;
	int steps;
	float time;
	float S(int index) { return index/(float)(steps-1); }
	float S(float index) { return CLAMP(index,0,steps)/(float)(steps-1); }
	float DS() { return 1/(float)(steps-1); }
	float DS(float leftIndex, float rightIndex) { return S(rightIndex)-S(leftIndex); }
	float IFS(float s) { return (int)(s*(steps-1)); }
public:
	Function2D(float adt, int aSteps):dt(adt),steps(aSteps > 1 ? aSteps : 2),ds(1/(float)(steps-1)),time(0){}
    virtual float tick() { return 0; }
private:
    //set delta t and delta s
    //tick
    //getPointsAtCurrentTime
	
};
